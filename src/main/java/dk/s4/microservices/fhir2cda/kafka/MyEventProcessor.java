package dk.s4.microservices.fhir2cda.kafka;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import dk.s4.microservices.fhir2cda.convert.Fhir2phmrConverter;
import dk.s4.microservices.fhir2cda.convert.Fhir2qrdConverter;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import org.hl7.fhir.r4.model.CodeableConcept;
import org.hl7.fhir.r4.model.OperationOutcome;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MyEventProcessor implements EventProcessor {
    private final Logger logger = LoggerFactory.getLogger(MyEventProcessor.class);

    private final Fhir2phmrConverter fhir2phmrConverter;
    private final Fhir2qrdConverter fhir2qrdConverter;
    private final FhirContext fhirContext;

    public MyEventProcessor(FhirContext fhirContext, Fhir2phmrConverter fhir2phmrConverter, Fhir2qrdConverter fhir2qrdConverter) {
        this.fhir2phmrConverter = fhir2phmrConverter;
        this.fhir2qrdConverter = fhir2qrdConverter;
        this.fhirContext = fhirContext;
    }

    @Override
    public boolean processMessage(Topic consumedTopic,
                                  Message receivedMessage,
                                  Topic messageProcessedTopic,
                                  Message outgoingMessage) {
        logger.debug("Processing message from topic {}", consumedTopic.toString());

        try {
            String cdaDocument = null;
            if (consumedTopic.getDataType().equals("Observation")) {
                messageProcessedTopic.setDataType("PHMR");
                outgoingMessage.setBodyType("CDA")
                        .setContentVersion("DK-PHMR-1.3")
                        .setCorrelationId(receivedMessage.getCorrelationId())
                        .setTransactionId(receivedMessage.getTransactionId());
                cdaDocument = fhir2phmrConverter.convert(receivedMessage.getBody());
            }
            else if (consumedTopic.getDataType().equals("Questionnaire")) {
                messageProcessedTopic.setDataType("QRD");
                outgoingMessage.setBodyType("CDA")
                        .setContentVersion("DK-QRD-1.2")
                        .setCorrelationId(receivedMessage.getCorrelationId())
                        .setTransactionId(receivedMessage.getTransactionId());
                cdaDocument = fhir2qrdConverter.convert(receivedMessage.getBody());
            }

            if (cdaDocument == null) {
                Exception e = new Exception("Error while processing FHIR ressource");
                onError(receivedMessage, outgoingMessage, e.getMessage());
                setKafkaTopic(messageProcessedTopic, consumedTopic, false);
                return false;
            }
            outgoingMessage.setBody(cdaDocument);
        } catch(Exception e) {
            logger.error("Processing failed: {}", e.getMessage(), e);
            onError(receivedMessage, outgoingMessage, e.getMessage());
            setKafkaTopic(messageProcessedTopic, consumedTopic, false);
            return false;
        }

        setKafkaTopic(messageProcessedTopic, consumedTopic, true);

        return true;
    }

    private void setKafkaTopic(Topic messageProcessedTopic, Topic consumedTopic,  boolean success) {
        messageProcessedTopic
                .setOperation(Topic.Operation.DataConverted)
                .setDataCategory(Topic.Category.CDA)
                .setDataCodes(consumedTopic.getDataCodes());

        if (!success) messageProcessedTopic.setOperation(Topic.Operation.ProcessingFailed);
    }

    private void onError(Message receivedMessage, Message outgoingMessage, String errorMsg) {

        IParser parser = fhirContext.newJsonParser();

        OperationOutcome outcome = new OperationOutcome();
        outcome.addIssue()
                .setSeverity(OperationOutcome.IssueSeverity.ERROR)
                .setDetails(new CodeableConcept()
                        .setText(errorMsg));
        outgoingMessage
                .setBody(parser.encodeResourceToString(outcome));
    }
}