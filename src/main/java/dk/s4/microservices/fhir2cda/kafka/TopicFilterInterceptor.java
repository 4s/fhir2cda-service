package dk.s4.microservices.fhir2cda.kafka;

import ca.uhn.fhir.rest.server.exceptions.AuthenticationException;
import dk.s4.microservices.messaging.EventConsumerInterceptorAdaptor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TopicFilterInterceptor extends EventConsumerInterceptorAdaptor {

    private static final Logger logger = LoggerFactory.getLogger(TopicFilterInterceptor.class);

    @Override
    public boolean incomingMessagePreProcessMessage(Topic consumedTopic, Message receivedMessage) throws AuthenticationException {
        if (!consumedTopic.getDataCodes().isEmpty() && consumedTopic.getDataCodes().get(0).equals("MDC8450059")) {
            logger.debug("Ignoring message with CTG data (Converting CTG is not supported)");
            return false;
        }

        return true;
    }
}
