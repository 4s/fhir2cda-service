package dk.s4.microservices.fhir2cda.service;

import dk.s4.microservices.microservicecommon.Env;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Enumeration of all environment variables, this service uses. Some variables are required while others are
 * optional.
 * This class ensures consistent behavior when accessing these variables, and convenience for validation and
 * access.
 */
public enum ServiceVariables {
    SERVICE_NAME("SERVICE_NAME", true),
    FHIR_VERSION("FHIR_VERSION", true),
    CORRELATION_ID("CORRELATION_ID", true),
    TRANSACTION_ID("TRANSACTION_ID", true),
    ENABLE_KAFKA("ENABLE_KAFKA",true),
    OUTCOME_SERVICE_URL("OUTCOME_SERVICE_URL", true),
    PATIENTCARE_SERVICE_URL("PATIENTCARE_SERVICE_URL", true),
    ORGANIZATIONAL_SERVICE_URL("ORGANIZATIONAL_SERVICE_URL", true),
    OUTCOMEDEFINITION_SERVICE_URL("OUTCOMEDEFINITION_SERVICE_URL", true),
    OFFICIAL_OBSERVATION_IDENTIFIER_SYSTEM("OFFICIAL_OBSERVATION_IDENTIFIER_SYSTEM", true),
    OFFICIAL_PATIENT_IDENTIFIER_SYSTEM("OFFICIAL_PATIENT_IDENTIFIER_SYSTEM", true),
    ORGANIZATION_IDENTIFIER_SYSTEM("ORGANIZATION_IDENTIFIER_SYSTEM", true),
    UCUM_SYSTEM("UCUM_SYSTEM", true),
    MEDCOM_OID("MEDCOM_OID", true),
    MDC_OID("MDC_OID", true),
    MDC_URN("MDC_URN", true),
    PHG_OBSERVATION_EXTENSION_URL("PHG_OBSERVATION_EXTENSION_URL", true),
    OBSERVATION_DATA_ENTRY_METHOD_EXTENSION_URL("OBSERVATION_DATA_ENTRY_METHOD_EXTENSION_URL", true),
    OBSERVATION_DATA_ENTRY_METHOD_CODING_SYSTEM("OBSERVATION_DATA_ENTRY_METHOD_CODING_SYSTEM", true),
    CUSTODIAN_ORGANIZATION("CUSTODIAN_ORGANIZATION", true),
    LEGAL_AUTHENTICATOR_ORGANIZATOIN("LEGAL_AUTHENTICATOR_ORGANIZATOIN", true),
    QRD_ID_ASSIGNING_AUTHORITY_NAME("QRD_ID_ASSIGNING_AUTHORITY_NAME", true),
    QRD_ID_ROOT("QRD_ID_ROOT", true),
    QRD_CODE_CODE_SYSTEM("QRD_CODE_CODE_SYSTEM", true),
    QRD_CODE_SYSTEM_NAME("QRD_CODE_SYSTEM_NAME", true),
    FHIR_QUESTIONNAIRE_CANNONICAL_LOOKUP_URL("FHIR_QUESTIONNAIRE_CANNONICAL_LOOKUP_URL", true),
    LEGAL_AUTHENTICATOR_PRACTITIONER("LEGAL_AUTHENTICATOR_PRACTITIONER", false),
    ENABLE_AUTHENTICATION("ENABLE_AUTHENTICATION", false),
    ENABLE_DIAS_AUTHENTICATION("ENABLE_DIAS_AUTHENTICATION", false),
    USER_CONTEXT_SERVICE_URL("USER_CONTEXT_SERVICE_URL", false),
    SERVICE_INPUT_TOPIC_PATTERN_STRING("SERVICE_INPUT_TOPIC_PATTERN_STRING", false),
    TEST_WRITE_XML_TO_FILE("TEST_WRITE_XML_TO_FILE", false);

    private final String key;
    private final boolean required;

    ServiceVariables(String key, boolean required) {
        this.key = key;
        this.required = required;
    }

    public static void registerAndEnsurePresence() {
        List<String> requiredKeys = variablesWith(v -> v.required).stream().map(ServiceVariables::getKey).collect(Collectors.toList());
        Env.registerRequiredEnvVars(requiredKeys);
        Env.checkEnv();
    }

    private static List<ServiceVariables> variablesWith(Predicate<ServiceVariables> predicate) {
        return Arrays.stream(ServiceVariables.values()).filter(predicate).collect(Collectors.toList());
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        String value = System.getenv(getKey());
        if (value == null || value.isEmpty()) {
            if (required) {
                throw new IllegalArgumentException("Required key not present: " + getKey());
            } else {
                return null;
            }
        }
        return value;
    }

    public boolean isSetToTrue() {
        String value = getValue();
        if (value == null) {
            return false;
        }
        return value.equalsIgnoreCase("true");
    }
}
