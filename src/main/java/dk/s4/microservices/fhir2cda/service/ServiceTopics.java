package dk.s4.microservices.fhir2cda.service;

import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ServiceTopics {
    private final static Logger logger = LoggerFactory.getLogger(ServiceTopics.class);

    public static Pattern get(){
        String topicPatternString = ServiceVariables.SERVICE_INPUT_TOPIC_PATTERN_STRING.getValue();
        if( topicPatternString == null) {
            topicPatternString = new Topic()
                    .setOperation(Operation.DataCreated)
                    .setDataCategory(Category.FHIR).toString() +
                    Topic.separator + "Observation(" + Topic.separator + ".*)*|"
                    + new Topic()
                    .setOperation(Operation.DataUpdated)
                    .setDataCategory(Category.FHIR).toString() +
                    Topic.separator + "Observation(" + Topic.separator + ".*)*|"
                    + new Topic()
                    .setOperation(Operation.DataCreated)
                    .setDataCategory(Category.FHIR).toString() +
                    Topic.separator + "Questionnaire|"
                    + new Topic()
                    .setOperation(Operation.DataUpdated)
                    .setDataCategory(Category.FHIR).toString() +
                    Topic.separator + "Questionnaire";
        }

        logger.info("Service topic pattern: " + topicPatternString);

        return Pattern.compile(topicPatternString);
    }
}
