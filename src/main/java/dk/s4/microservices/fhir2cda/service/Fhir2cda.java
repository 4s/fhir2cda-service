package dk.s4.microservices.fhir2cda.service;

import ca.uhn.fhir.context.FhirContext;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.microservices.fhir2cda.convert.FHIR2CDAUtils;
import dk.s4.microservices.fhir2cda.convert.FHIRRestUtils;
import dk.s4.microservices.fhir2cda.convert.Fhir2qrdConverter;
import dk.s4.microservices.fhir2cda.convert.FhirContextUtil;
import dk.s4.microservices.fhir2cda.convert.Fhir2phmrConverter;
import dk.s4.microservices.fhir2cda.kafka.MyEventProcessor;
import dk.s4.microservices.fhir2cda.kafka.TopicFilterInterceptor;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.kafka.KafkaConsumeAndProcess;
import dk.s4.microservices.messaging.kafka.KafkaEventProducer;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Fhir2Cda Service subscribes to Kafka messages containing FHIR Observation
 * resources, converts these resources to HL7 PHMR format and writes messages containing
 * the converted data back to Kafka.
 *
 */
public class Fhir2cda {
	private final static Logger logger = LoggerFactory.getLogger(Fhir2cda.class);

	public static Thread kafkaConsumeAndProcessThread;

	public static void main(String[] args)  {
		logger.debug("Starting fhir2cda-service");

		ServiceVariables.registerAndEnsurePresence();

		try {
			if (kafkaConsumeAndProcessThread == null) {
				KafkaEventProducer kafkaEventProducer = new KafkaEventProducer("Fhir2cda-service");
				FhirContext fhirContext = FhirContextUtil.getFhirContext();
				FHIRRestUtils restUtils = new FHIRRestUtils(fhirContext,
						ServiceVariables.OUTCOME_SERVICE_URL.getValue(),
						ServiceVariables.PATIENTCARE_SERVICE_URL.getValue(),
						ServiceVariables.ORGANIZATIONAL_SERVICE_URL.getValue(),
						ServiceVariables.OUTCOMEDEFINITION_SERVICE_URL.getValue());
				FHIR2CDAUtils cdaUtils = new FHIR2CDAUtils(fhirContext);
				Fhir2phmrConverter phmrConverter = new Fhir2phmrConverter(new PHMRXmlCodec(), fhirContext, restUtils, cdaUtils);
				Fhir2qrdConverter qrdConverter = new Fhir2qrdConverter(new QRDXmlCodec(), fhirContext, restUtils, cdaUtils);
				EventProcessor eventProcessor = new MyEventProcessor(fhirContext, phmrConverter, qrdConverter);
				KafkaConsumeAndProcess kafkaConsumeAndProcess = new KafkaConsumeAndProcess(ServiceTopics.get(), kafkaEventProducer, eventProcessor);
				kafkaConsumeAndProcess.registerInterceptor(new TopicFilterInterceptor());
				kafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess);
			}

			kafkaConsumeAndProcessThread.start();

		} catch (KafkaInitializationException | MessagingInitializationException e) {
			logger.error("Error during Kafka initialization: ", e);
			System.exit(1);
		}
	}
}
