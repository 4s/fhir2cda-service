package dk.s4.microservices.fhir2cda.convert;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.core.BaseClinicalDocument;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.microservices.fhir2cda.service.ServiceVariables;
import java.util.Date;
import java.util.List;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.DomainResource;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Reference;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Fhir2cdaConverter<T extends IBaseResource> {
    private final static Logger logger = LoggerFactory.getLogger(Fhir2cdaConverter.class);

    protected final FHIRRestUtils restUtils;
    protected final FHIR2CDAUtils cdaUtils;
    protected final FhirContext fhirContext;
    protected final Codec codec;

    public Fhir2cdaConverter(Codec codec, FhirContext fhirContext, FHIRRestUtils restUtils, FHIR2CDAUtils cdaUtils) {
        this.codec = codec;
        this.restUtils = restUtils;
        this.cdaUtils = cdaUtils;
        this.fhirContext = fhirContext;
    }

    /**
     * Converts a FHIR resource resource to HL7 CDA
     *
     * @param resourceString The FHIR  resource as a string to be converted into CDA
     * @return the CDA XML document as a string
     */
    public String convert(String resourceString) throws Exception {

        String cdaString = "";

        // Notice: we assume JSON, not XML for FHIR resources
        IParser parser = fhirContext.newJsonParser();

        IBaseResource fhirResource = parser.parseResource(resourceString);

        BaseClinicalDocument cda = buildHeader((T)fhirResource);
        if (cda == null)
            throw new Exception("Header build failed");
        buildBody(cda, (T)fhirResource);

        // PHMR to String
        cdaString = (String) codec.encode(cda);

        boolean completeCDA = cdaString.contains("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>")
                && cdaString.contains("</ClinicalDocument>");

        if (completeCDA) {
            logger.debug("Produced complete CDA document");
            logger.trace("CDA document: {}...", cdaString.substring(0, 2000));
        } else {
            throw new Exception("Conversion produced incomplete CDA document");
        }

        return cdaString;
    }

    /**
     * Build CDA header from FHIR  resource
     * @param fhirResource FHIR resource
     * @return CDA document with header filled
     * @throws Exception if subject can not be looked up or if CDA patient cannot be built
     */
    protected abstract BaseClinicalDocument buildHeader(T fhirResource) throws Exception;

    /**
     * Build CDA body from FHIR resource
     *
     * @param cda CDA document instance
     * @param fhirResource FHIR resource
     * @throws Exception
     */
    protected abstract void buildBody(BaseClinicalDocument cda, T fhirResource) throws Exception;

    protected void setCustodian(BaseClinicalDocument cda) {
        String custodianOrgString = ServiceVariables.CUSTODIAN_ORGANIZATION.getValue();
        Organization custodianOrganization = fhirContext.newJsonParser().parseResource(Organization.class, custodianOrgString);
        OrganizationIdentity custodianOrganizationIdentity = cdaUtils.buildOrganizationIdentity(custodianOrganization);
        cda.setCustodian(custodianOrganizationIdentity);
    }

    protected Organization getBasedOnOrganization(List<Reference> basedOnList) throws Exception {
        Identifier carePlanIdentifier = null;
        for (Reference basedOnRef: basedOnList) {
            if (basedOnRef.hasIdentifier() && basedOnRef.hasType() && basedOnRef.getType().equals("CarePlan")) {
                carePlanIdentifier = basedOnRef.getIdentifier();
            }
        }
        if (carePlanIdentifier == null)
            throw new Exception("Observation is not basedOn a CarePlan");
        CarePlan carePlan = restUtils.getCarePlan(carePlanIdentifier);
        if (carePlan == null)
            throw new Exception("Could not resolve CarePlan for Observation");

        //Get author organization from CarePlan
        if (!carePlan.hasAuthor() || !carePlan.getAuthor().hasIdentifier())
            throw new Exception("CarePlan for Observation is missing author");

        Organization organization = restUtils.getOrganization(carePlan.getAuthor().getIdentifier());
        if (organization == null)
            throw new Exception("Failed to get author organization");

        return organization;
    }
}
