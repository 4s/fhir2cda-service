package dk.s4.microservices.fhir2cda.convert;

import static dk.s4.hl7.cda.model.Reference.DocumentIdReferencesUse.HTTP_REFERENCE;


import ca.uhn.fhir.context.FhirContext;
import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.Telecom;
import dk.s4.hl7.cda.model.core.BaseClinicalDocument;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse.QRDNumericResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDResponse.BaseQRDResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse.QRDTextResponseBuilder;
import dk.s4.microservices.fhir2cda.service.ServiceVariables;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.Questionnaire.QuestionnaireItemComponent;
import org.hl7.fhir.r4.model.QuestionnaireResponse;
import org.hl7.fhir.r4.model.QuestionnaireResponse.QuestionnaireResponseItemComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Fhir2qrdConverter extends Fhir2cdaConverter<QuestionnaireResponse> {

    private final static Logger logger = LoggerFactory.getLogger(Fhir2qrdConverter.class);
    private Map<String, Questionnaire> questionnaireMap = new HashMap<>();

    public Fhir2qrdConverter(Codec codec, FhirContext fhirContext, FHIRRestUtils restUtils, FHIR2CDAUtils cdaUtils) {
        super(codec, fhirContext, restUtils, cdaUtils);
    }

    @Override
    protected BaseClinicalDocument buildHeader(QuestionnaireResponse fhirResource) throws Exception {
        Date documentCreationTime = new Date();
        QRDDocument qrd = initQrdDocument(fhirResource, getQuestionnaire(fhirResource), documentCreationTime);
        if (!fhirResource.hasAuthored())
            throw new Exception("FHIR QuestionnaireResponse must have authored atrribute");
        qrd.setDocumentationTimeInterval(fhirResource.getAuthored(), fhirResource.getAuthored());

        setPatient(qrd, fhirResource);
        setAuthorAndDataEnterer(qrd, fhirResource, documentCreationTime);
        setCustodian(qrd);
        setInformationRecipients(qrd, fhirResource, documentCreationTime);

        return qrd;
    }

    private void buildAnswerItem(Section<QRDResponse> section, QuestionnaireResponse questionnaireResponse, QuestionnaireResponseItemComponent rItem, QuestionnaireItemComponent qItem)
            throws Exception {
        BaseQRDResponseBuilder responseBuilder = null;
        Questionnaire.QuestionnaireItemType questionType = qItem.getType();
        switch (questionType) {
            case QUANTITY:
                responseBuilder = new QRDNumericResponseBuilder();
                ((QRDNumericResponseBuilder)responseBuilder).setValue(rItem.getAnswerFirstRep().getValueQuantity().getValue().toString(), BasicType.REAL);
                break;
            case BOOLEAN:
                responseBuilder = new QRDNumericResponseBuilder();
                int value = (Boolean.parseBoolean(rItem.getAnswerFirstRep().getValueBooleanType().getValueAsString())) ? 1 : 0;
                ((QRDNumericResponseBuilder)responseBuilder).setValue(String.valueOf(value), BasicType.INT);
                break;
            case INTEGER:
                responseBuilder = new QRDNumericResponseBuilder();
                ((QRDNumericResponseBuilder)responseBuilder).setValue(rItem.getAnswerFirstRep().getValueIntegerType().getValueAsString(), BasicType.INT);
                break;
            case DECIMAL:
                responseBuilder = new QRDNumericResponseBuilder();
                ((QRDNumericResponseBuilder)responseBuilder).setValue(rItem.getAnswerFirstRep().getValueDecimalType().getValueAsString(), BasicType.REAL);
                break;
            case TEXT: case STRING: case URL: case DATE: case DATETIME: case TIME: case QUESTION: case OPENCHOICE: case CHOICE:
                responseBuilder = new QRDTextResponseBuilder();
                ((QRDTextResponseBuilder)responseBuilder).setText(rItem.getAnswerFirstRep().getValue().toString());
                break;
            case DISPLAY: default:
                break;
        }
        if (responseBuilder != null) {
            ID id = new IDBuilder()
                    .setAuthorityName(ServiceVariables.QRD_ID_ASSIGNING_AUTHORITY_NAME.getValue())
                    .setRoot(ServiceVariables.QRD_ID_ROOT.getValue())
                    .setExtension(qItem.getLinkId()).build();
            CodedValue codedValue = new CodedValueBuilder()
                    .setCodeSystem(ServiceVariables.QRD_CODE_CODE_SYSTEM.getValue())
                    .setCodeSystemName(ServiceVariables.QRD_CODE_SYSTEM_NAME.getValue())
                    .setDisplayName(qItem.getLinkId() + " spørgsmål")
                    .setCode(qItem.getLinkId()).build();
            responseBuilder
                    .setQuestion(qItem.getText())
                    .setId(id)
                    .setCodeValue(codedValue);

            //Construct externalObject reference to linkId in FHIR Questionnaire
            String searchUrl = restUtils.getSearchUrlFromCannonical(questionnaireResponse.getQuestionnaire());
            ID externalDocId = new IDBuilder()
                    .setRoot("1.2.208.184.5.3")
                    .setExtension(ServiceVariables.FHIR_QUESTIONNAIRE_CANNONICAL_LOOKUP_URL.getValue() + searchUrl).build();
            CodedValue externalDocCode = new CodedValueBuilder()
                    .setCode("Questionnaire")
                    .setCodeSystem("2.16.840.1.113883.6.306")
                    .setDisplayName("fhir-resource-types").build();
            ID externalObsId = new IDBuilder()
                    .setRoot("1.2.208.184.100.2")
                    .setExtension(qItem.getLinkId()).build();
            Reference extObjectRef = new ReferenceBuilder(HTTP_REFERENCE.getReferencesUse(), externalDocId, externalDocCode).externalObservation(externalObsId).build();
            responseBuilder.addReference(extObjectRef);

            QRDResponse response = responseBuilder.build();
            section.addQuestionnaireEntity(response);
        }
    }

    private void buildItems(Section<QRDResponse> section, QuestionnaireResponse questionnaireResponse,
            List<QuestionnaireResponseItemComponent> responseItems, List<QuestionnaireItemComponent> questionnaireItems) throws Exception {
        for(QuestionnaireResponseItemComponent rItem: responseItems) {
            logger.debug("responseItems LinkId = " + rItem.getLinkId() + " rItem.hasAnswer = " + rItem.hasAnswer());
            for(QuestionnaireItemComponent qItem: questionnaireItems) {
                if(rItem.getLinkId().equals(qItem.getLinkId())) {
                    logger.debug("LinkId = " + qItem.getLinkId() + " item type = " + qItem.getType().name());
                    if (rItem.hasItem())
                        buildItems(section, questionnaireResponse, rItem.getItem(), qItem.getItem());
                    if (rItem.hasAnswer())
                        buildAnswerItem(section, questionnaireResponse, rItem, qItem);
                }
            }
        }

    }

    @Override
    protected void buildBody(BaseClinicalDocument cda, QuestionnaireResponse questionnaireResponse) throws Exception {
        QRDDocument qrd = (QRDDocument)cda;
        Questionnaire questionnaire = getQuestionnaire(questionnaireResponse);
        if (qrd != null) {

            String title = (!questionnaire.getTitle().isEmpty()) ? questionnaire.getTitle() : "No title";
            Section<QRDResponse> section = new Section<>(title, title);

            List<QuestionnaireResponse.QuestionnaireResponseItemComponent> responseItems = questionnaireResponse.getItem();
            if (!responseItems.isEmpty())
                buildItems(section, questionnaireResponse, responseItems, questionnaire.getItem());
            qrd.addSection(section);
        }
    }

    protected Questionnaire getQuestionnaire(QuestionnaireResponse questionnaireResponse) throws Exception{
        Questionnaire questionnaire = questionnaireMap.get(questionnaireResponse.getQuestionnaire());
        if (questionnaire == null) {
            questionnaire = restUtils.getQuestionnaire(questionnaireResponse.getQuestionnaire());
            if (questionnaire == null)
                throw new Exception("Could not retrieve Questionnaire for QuestionnaireResponse");
        }
        return questionnaire;
    }

    private QRDDocument initQrdDocument(QuestionnaireResponse questionnaireResponse, Questionnaire questionnaire, Date documentCreationTime) {
        // Use a QuestionnaireResponse identifier as QRD id. Either one in the MedCom OID or one in the official QuestionnaireResponse.identifier system.
        // If none of these can be found use a randow UUID.
        String documentId = "";
        Identifier identifier = questionnaireResponse.getIdentifier();
        if (identifier.hasSystem()) {
            if (identifier.getSystem().equals(ServiceVariables.MEDCOM_OID.getValue())) {
                documentId = identifier.getValue();
            }
            else if (identifier.getSystem().equals(ServiceVariables.OFFICIAL_OBSERVATION_IDENTIFIER_SYSTEM.getValue())) {
                documentId = identifier.getValue();
            }
        }
        if (documentId != null && documentId.isEmpty())
            documentId = UUID.randomUUID().toString();

        QRDDocument qrd = new QRDDocument(new ID.IDBuilder()
                .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
                .setExtension(documentId)
                .setRoot(MedCom.ROOT_OID).build()
        );

        // Time when this document is created. Not based on values from FHIR resources.
        qrd.setEffectiveTime(documentCreationTime);
        qrd.setLanguageCode("da-DK");

        // title
        String title = "Spørgeskema uden titel";
        if (questionnaire.getTitle() != null) {
            title = questionnaire.getTitle();
        }
        qrd.setTitle(title);

        return qrd;
    }

    private void setPatient(QRDDocument qrd, QuestionnaireResponse questionnaireResponse) throws Exception {
        if (!questionnaireResponse.hasSubject() || !questionnaireResponse.getSubject().hasIdentifier())
            throw new Exception("QuestionnaireResponse is missing subject");

        org.hl7.fhir.r4.model.Patient patient = restUtils.getPatient(questionnaireResponse.getSubject().getIdentifier());
        if (patient == null || patient.isEmpty())
            throw new Exception("Patient is null or empty");

        dk.s4.hl7.cda.model.Patient cdaPatient = cdaUtils.buildPatient(patient);
        if (cdaPatient == null)
            throw new Exception("Building CDA patient failed");

        qrd.setPatient(cdaPatient);

    }

    private void setAuthorAndDataEnterer(QRDDocument qrd, QuestionnaireResponse questionnaireResponse, Date documentCreationTime) throws Exception {
        // In QRD the author is the person who answers the questions)

        AddressData addressData = null;
        List<Telecom> telecomList = null;

        if (!questionnaireResponse.hasSource() || !questionnaireResponse.getSource().hasIdentifier())
            throw new Exception("QuestionnaireResponse is missing source attribute");

        if (!questionnaireResponse.getSource().hasType() && questionnaireResponse.getSource().getType().equals("Patient"))
            throw new Exception("Only Patient type source is supported on QuestionnaireResponse");

        Patient patient = restUtils.getPatient(questionnaireResponse.getSource().getIdentifier());
        PersonIdentity personIdentity = cdaUtils.buildPersonIdentity(patient);
        addressData = cdaUtils.buildAddressData(patient.getAddressFirstRep());
        telecomList = cdaUtils.buildTelecomList(patient.getTelecom());

        Participant.ParticipantBuilder participantBuilder = new Participant.ParticipantBuilder();
        if (personIdentity != null) {
            participantBuilder
                    .setPersonIdentity(personIdentity)
                    .setTime(documentCreationTime)
                    .setAddress(addressData);
            if (telecomList != null)
                for(Telecom telecom: telecomList)
                    participantBuilder.addTelecom(telecom.getAddressUse(), telecom.getProtocol(), telecom.getValue());

        }

        Participant participant = participantBuilder.build();
        qrd.setAuthor(participant);
        qrd.setDataEnterer(participant);
    }

    private void setInformationRecipients(QRDDocument qrd, QuestionnaireResponse questionnaireResponse, Date documentCreationTime) throws Exception {
        if (!questionnaireResponse.hasBasedOn())
            throw new Exception("QuestionnaireResponse is missing basedOn");
        Organization organization = getBasedOnOrganization(questionnaireResponse.getBasedOn());
        OrganizationIdentity organisationIdentity = cdaUtils.buildOrganizationIdentity(organization);
        Participant participant = cdaUtils.buildParticipant(organisationIdentity, null, documentCreationTime);
        qrd.addInformationRecipients(participant);
    }
}
