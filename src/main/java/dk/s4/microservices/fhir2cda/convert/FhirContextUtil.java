package dk.s4.microservices.fhir2cda.convert;

import ca.uhn.fhir.context.FhirContext;

public class FhirContextUtil {

    private static FhirContext instance = null;

    public static FhirContext getFhirContext() {
        if(instance == null) {
            instance = FhirContext.forR4();
        }
        return instance;
    }
}