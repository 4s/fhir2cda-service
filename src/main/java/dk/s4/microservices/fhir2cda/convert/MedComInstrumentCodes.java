package dk.s4.microservices.fhir2cda.convert;

import ca.uhn.fhir.rest.annotation.Read;
import com.sun.xml.bind.v2.model.annotation.Quick;
import org.apache.commons.collections4.map.MultiKeyMap;

/**
 * Map of MedCom instrument codes. Source: https://svn.medcom.dk/svn/releases/Standarder/POCT-Hjemmemonitorering/MedCom%20Instrument%20Codes.xlsx
 * Notice: Corrected what I assume to be a spelling mistake: "Simens" -> "Siemens"
 */
public class MedComInstrumentCodes {

    public static final MultiKeyMap map =new MultiKeyMap() {
        {
            put("A&D Company","AD-6121ABT1","MCI00001");
            put("A&D Company", "UC-321PBT-C", "MCI00002");
            put("A&D Company", "UC-355PBT-Ci", "MCI00003");
            put("A&D Medical", "UA-767PlusBT-C Bluetooth", "MCI00004");
            put("Nonin", "Onyx II 9560 bluetooth oximeter", "MCI00005");
            put("Vitalograph", "Lung Monitor Bluetooth", "MCI00006");
            put("Roche Diagnostics","Urisys 1100", "MCI00007");
            put("Siemens", "Clinitek Status+", "MCI00008");
            put("A&D Company", "UC352BLE", "MCI00009");
            put("A&D Medical","UT-302PlusBT Bluetooth", "MCI00010");
            put("A&D Medical","UA-351-PBT-Ci Bluetooth", "MCI00011");
            put("A&D Medical","UA-767PlusBT-Ci Bluetooth", "MCI00012");
            put("Nonin", "3230 Bluetooth Smart Pulse Oximeter", "MCI00013");
            put("Vitalograph", "4000 Lung Monitor Bluetooth", "MCI00014");
            put("Alere", "Afinion AS100", "MCI00015");
            put("Siemens", "DCA Vantage Analyzer", "MCI00016");
            put("Hemocue", "WBC DIFF", "MCI00017");
            put("Hemocue", "Hb 201+", "MCI00018");
            put("Hemocue", "Hb 201 RT", "MCI00019");
            put("Hemocue", "Glukose 201", "MCI00020");
            put("Hemocue", "Glukose 201RT", "MCI00021");
            put("Roche Diagnostics", "CoaguChek", "MCI00022");
            put("Macherey-Nagel", "URYXXON Relax", "MCI00023");
            put("Orion Diagnostica", "Quick Read Go", "MCI00024");
            put("EKF Diagnostic", "Quo-Test", "MCI00025");
            put("Bodytech", "i-CHROMA", "MCI00026");
            put("Alere", "Urilyzer 100", "MCI00027");
        }
    };
}
