package dk.s4.microservices.fhir2cda.convert;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import ca.uhn.fhir.rest.gclient.TokenClientParam;
import ca.uhn.fhir.rest.param.UriAndListParam;
import ca.uhn.fhir.rest.param.UriOrListParam;
import ca.uhn.fhir.rest.param.UriParam;
import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import dk.s4.microservices.fhir2cda.service.ServiceVariables;
import dk.s4.microservices.microservicecommon.fhir.ExceptionUtils;
import dk.s4.microservices.microservicecommon.fhir.MyFhirClientFactory;
import org.apache.kafka.common.protocol.types.Field.Str;
import org.hl7.fhir.r4.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FHIRRestUtils {
    private final static Logger logger = LoggerFactory
            .getLogger(FHIRRestUtils.class);

    private final FhirContext fhirContext;
    private final String outcomeServiceUrl;
    private final String patientCareServiceUrl;
    private final String organizationalServiceUrl;
    private final String outcomeDefinitionServiceUrl;

    public FHIRRestUtils(FhirContext fhirContext, String outcomeServiceUrl, String patientCareServiceUrl, String organizationalServiceUrl, String outcomeDefinitionServiceUrl) {
        this.fhirContext = fhirContext;
        this.outcomeServiceUrl = outcomeServiceUrl;
        this.patientCareServiceUrl = patientCareServiceUrl;
        this.organizationalServiceUrl = organizationalServiceUrl;
        this.outcomeDefinitionServiceUrl = outcomeDefinitionServiceUrl;
    }

    /**
     * Retrieves the FHIR patient resource specified by identifier
     * @param identifier Patient identifier
     * @return FHIR patient resource
     * @throws Exception if lookup of patient fails
     */
    public Patient getPatient(Identifier identifier) throws Exception {
        logger.debug("getPatient");
        Patient patient = null;

        IGenericClient patientClient = MyFhirClientFactory.getInstance().getClientFromBaseUrl(patientCareServiceUrl, fhirContext);

        String searchUrl = patientCareServiceUrl +
                "/Patient?_query=searchByIdentifier&" +
                "identifier=" + identifier.getSystem() + "|" + identifier.getValue();
        Bundle bundle = patientClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

        if (!bundle.hasEntry() || bundle.getEntry().isEmpty() || !bundle.getEntry().get(0).hasResource())
            throw new Exception("Failed patient lookup on url: " + searchUrl);

        return (Patient) bundle.getEntry().get(0).getResource();
    }

    /**
     * Retieves FHIR Device resource from external outcome-service.
     *
     * @param deviceReference Logical FHIR reference to Device
     * @return FHIR Device retrieved
     * @throws ResourceNotFoundException if the resource can not be found on external service
     * @throws Exception if input or returned result from external service has unexpected format
     */
    public Device getDevice(Reference deviceReference) throws ResourceNotFoundException, Exception {
        Identifier identifier = deviceReference.getIdentifier();

        logger.debug("getDevice");

        if (identifier.hasSystem() == false || identifier.hasValue() == false) {
            throw new Exception("Device identifier with system and value not found");
        }

        IGenericClient deviceClient = MyFhirClientFactory.getInstance().getClientFromBaseUrl(outcomeServiceUrl, FhirContextUtil.getFhirContext());

        String searchUrl = outcomeServiceUrl
                + "/Device?_query=getByIdentifier&identifier="
                + identifier.getSystem()
                + "|" + identifier.getValue();
        Bundle bundle = deviceClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();

        Device lookedUpDevice = null;
        if (bundle.hasEntry() && !bundle.getEntry().isEmpty()) {
            if (!bundle.getEntry().get(0).hasResource() || bundle.getEntry().get(0).isEmpty()) {
                throw new Exception("Found entry with no resource");
            }

            lookedUpDevice = (Device) bundle.getEntry().get(0).getResource();
            if (!lookedUpDevice.hasIdentifier())
                throw new Exception("Found device with no identifier - should not happen");
        }

        return lookedUpDevice;
    }

    /**
     * Retrieves FHIR CarePlan from external patientCare-service using CarePlan identifier.
     *
     * @param identifier of CarePlan
     * @return FHIR CarePlan
     * @throws ResourceNotFoundException if the resource can not be found on external service
     */
    public CarePlan getCarePlan(Identifier identifier) throws ResourceNotFoundException {
        logger.debug("Getting CarePlan from " + patientCareServiceUrl);
        CarePlan carePlan = null;

        IGenericClient patientCareClient = MyFhirClientFactory.getInstance().getClientFromBaseUrl(patientCareServiceUrl, FhirContextUtil.getFhirContext());
        Bundle searchRes = patientCareClient.search().forResource(CarePlan.class)
                .where(new TokenClientParam("identifier").exactly().systemAndCode(identifier.getSystem(), identifier.getValue()))
                .returnBundle(Bundle.class)
                .execute();
        if (searchRes != null && searchRes.hasEntry() && searchRes.getEntry().get(0).hasResource()) {
            carePlan = (CarePlan)searchRes.getEntry().get(0).getResource();
        }
        else
            logger.warn("No CarePlan found at " + patientCareServiceUrl);

        return carePlan;
    }

    /**
     * Retrieves FHIR Organization from external organizational-service using Organization identifier.
     *
     * @param identifier of Organization
     * @return FHIR Organization
     * @throws ResourceNotFoundException if the resource can not be found on external service
     */
    public Organization getOrganization(Identifier identifier) throws ResourceNotFoundException {
        logger.debug("Getting Organization from " + organizationalServiceUrl);
        Organization organization  = null;

        IGenericClient patientCareClient = MyFhirClientFactory.getInstance().getClientFromBaseUrl(organizationalServiceUrl, FhirContextUtil.getFhirContext());
        Bundle searchRes = patientCareClient.search().forResource(Organization.class)
                .where(new TokenClientParam("identifier").exactly().systemAndCode(identifier.getSystem(), identifier.getValue()))
                .returnBundle(Bundle.class)
                .execute();
        if (searchRes != null && searchRes.hasEntry() && searchRes.getEntry().get(0).hasResource()) {
            organization = (Organization) searchRes.getEntry().get(0).getResource();
        }
        else
            logger.warn("No Organization found at " + organizationalServiceUrl);

        return organization;
    }

    /**
     * Retrieves FHIR Questionnaire from external outcomedefinition-service using cannnonical url.
     *
     * Example input: "canonical://uuid/Questionnaire/2761fbe9-f4d0-49c9-b96e-ae1129d1d4aa|1.0.0"
     *
     * @param questionnaireCanonical Cannonical url of Questionnaire
     * @throws ResourceNotFoundException if the resource can not be found on external service
     * @throws Exception if questionnaireCanonical is malformed
     */
    public Questionnaire getQuestionnaire(String questionnaireCanonical) throws Exception {
        logger.debug("Getting Questionnaire from " + outcomeDefinitionServiceUrl);
        Questionnaire questionnaire = null;

        String searchUrl = getSearchUrlFromCannonical(questionnaireCanonical);
        IGenericClient outcomeDefClient = MyFhirClientFactory.getInstance().getClientFromBaseUrl(outcomeDefinitionServiceUrl, FhirContextUtil.getFhirContext());
        Bundle searchRes = outcomeDefClient.search().byUrl(searchUrl).returnBundle(Bundle.class).execute();
        if (searchRes != null && searchRes.hasEntry() && searchRes.getEntry().get(0).hasResource()) {
            questionnaire = (Questionnaire) searchRes.getEntry().get(0).getResource();
        }
        else
            logger.warn("No Questionnaire found at " + outcomeDefinitionServiceUrl);

        return questionnaire;
    }

    /**
     * Construct search url from a cannonical URL.
     *
     * Example input: "canonical://uuid/Questionnaire/2761fbe9-f4d0-49c9-b96e-ae1129d1d4aa|1.0.0"
     *
     * @param cannonical Cannonical URL
     * @return String containing search URL
     * @throws Exception if cannonical is malformed
     */
    public String getSearchUrlFromCannonical(String cannonical) throws Exception {
        String[] splitCanon = cannonical.split("\\|");
        if (splitCanon.length != 2 || splitCanon[0].isEmpty() || splitCanon[1].isEmpty())
            throw new Exception("Malformed cannonical url for Questionnaire");
        return "Questionnaire?version=" + splitCanon[1] + "&url=" + splitCanon[0];
    }
}
