package dk.s4.microservices.fhir2cda.convert;

import ca.uhn.fhir.context.FhirContext;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.DataInputContext.PerformerType;
import dk.s4.hl7.cda.model.DataInputContext.ProvisionMethod;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment.MedicalEquipmentBuilder;
import dk.s4.microservices.fhir2cda.service.ServiceVariables;
import java.util.UUID;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.r4.model.*;
import org.hl7.fhir.r4.model.Device.DeviceVersionComponent;
import org.hl7.fhir.r4.model.Observation.ObservationComponentComponent;
import org.hl7.fhir.r4.model.Observation.ObservationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FHIR2CDAUtils {
    private final static Logger logger = LoggerFactory
            .getLogger(FHIR2CDAUtils.class);

    private final FhirContext fhirContext;

    public FHIR2CDAUtils(FhirContext fhirContext) {
        this.fhirContext = fhirContext;
    }

    /**
     * Based on FHIR Practitioner resource builds PersonIdentity information for PHMR
     *
     * @param practitioner FHIR Practitioner resource
     * @return PersonIdentity
     */
    public PersonIdentity buildPersonIdentity(org.hl7.fhir.r4.model.Practitioner practitioner) {
        PersonIdentity personIdentity;

        PersonIdentity.PersonBuilder personBuilder = new PersonIdentity.PersonBuilder(
            practitioner.getNameFirstRep().getFamily());
        for(StringType givenName : practitioner.getNameFirstRep().getGiven()) {
            personBuilder.addGivenName(givenName.getValue());
        }
        personIdentity = personBuilder.setPrefix(practitioner.getNameFirstRep().getPrefixAsSingleString()).build();

        return personIdentity;
    }

    /**
     * Based on FHIR Patient resource builds PersonIdentity information for PHMR
     *
     * @param patient FHIR Patient resource
     * @return PersonIdentity
     */
    public PersonIdentity buildPersonIdentity(org.hl7.fhir.r4.model.Patient patient) {
        PersonIdentity personIdentity;

        PersonIdentity.PersonBuilder personBuilder = new PersonIdentity.PersonBuilder(
                patient.getNameFirstRep().getFamily());
        for(StringType givenName : patient.getNameFirstRep().getGiven()) {
            personBuilder.addGivenName(givenName.getValue());
        }
        personIdentity = personBuilder.setPrefix(patient.getNameFirstRep().getPrefixAsSingleString()).build();

        return personIdentity;
    }

    /**
     * Based on FHIR patient resource builds Patient information for PHMR
     *
     * @param patient
     *            FHIR Patient resource
     * @return Patient
     */
    public dk.s4.hl7.cda.model.Patient buildPatient(org.hl7.fhir.r4.model.Patient patient) {

        logger.debug("Building CDA Patient from FHIR Patient: "
                + fhirContext.newJsonParser().setPrettyPrint(true)
                .encodeResourceToString(patient));
        dk.s4.hl7.cda.model.Patient pt;

        String patientCpr = "";

        List<String> givenNames = new ArrayList<>();
        String familyName = "";
        List<Telecom> telecomList = null;
        AddressData addressData = null;
        Date birthDate = null;

        List<Identifier> identifierList = patient.getIdentifier();
        for (Identifier identifier : identifierList) {
            // Look for DK CPR identifier
            if (identifier.getSystem().equalsIgnoreCase(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue())) {
                patientCpr = patient.getIdentifier().get(0).getValue();
            }
        }

        if (!patientCpr.isEmpty()) {
            for(StringType given: patient.getName().get(0).getGiven()) {
                givenNames.add(given.toString());
            }

            familyName = patient.getName().get(0).getFamily();
            addressData = buildAddressData(patient.getAddressFirstRep());

            telecomList = buildTelecomList(patient.getTelecom());

            birthDate = patient.getBirthDate();
        }


        Patient.PatientBuilder patientBuilder = new dk.s4.hl7.cda.model.Patient.PatientBuilder(familyName);
                for(String given: givenNames) {
                    patientBuilder.addGivenName(given);
                }
                patientBuilder.setBirthTime(birthDate)
                .setSSN(patientCpr)
                .setAddress(addressData);

        if (telecomList != null && !telecomList.isEmpty()) {
            for(Telecom telecom: telecomList) {
                patientBuilder.addTelecom(telecom.getAddressUse(), telecom.getProtocol(), telecom.getValue());
            }
        }

        pt = patientBuilder.build();

        return pt;
    }

    /**
     * Based on FHIR organization resource builds OrganizationIdentity information for PHMR
     *
     * Using mock data until organisation services are in place
     *
     * @return OrganizationIdentity
     */
    public OrganizationIdentity buildOrganizationIdentity(Organization organization) {

        logger.debug("Building CDA Organization from FHIR Organization: "
                + fhirContext.newJsonParser().setPrettyPrint(true)
                .encodeResourceToString(organization));

        OrganizationIdentity organizationIdentity;

        String SOR = null;
        for (Identifier identifier : organization.getIdentifier()) {
            if (identifier.getSystem().equals(ServiceVariables.ORGANIZATION_IDENTIFIER_SYSTEM.getValue())) {
                SOR = identifier.getValue();
            }
        }

        AddressData organisationAddress = null;
        if (!organization.getAddress().isEmpty())
            organisationAddress = buildAddressData(organization.getAddressFirstRep());

        List<Telecom> telecomList = buildTelecomList(organization.getTelecom());

        OrganizationIdentity.OrganizationBuilder organizationBuilder = new OrganizationIdentity.OrganizationBuilder()
                .setSOR(SOR)
                .setName(organization.getName())
                .setAddress(organisationAddress);

        if (!telecomList.isEmpty()) {
            for(Telecom telecom: telecomList) {
                organizationBuilder.addTelecom(telecom.getAddressUse(), telecom.getProtocol(), telecom.getValue());
            }
        }

        organizationIdentity = organizationBuilder.build();

        return organizationIdentity;
    }

    public Participant buildParticipant(OrganizationIdentity organizationIdentity, PersonIdentity personIdentity, Date time) {
        ParticipantBuilder participantBuilder = new ParticipantBuilder();
        participantBuilder
                .setOrganizationIdentity(organizationIdentity)
                .setPersonIdentity(personIdentity)
                .setTime(time)
                .setId(organizationIdentity != null ? organizationIdentity.getId() : null)
                .setAddress(organizationIdentity.getAddress())
                .setTelecomList(organizationIdentity.getTelecomList().length == 0 ? null : organizationIdentity.getTelecomList());
        return participantBuilder.build();
    }

    /**
     *  Based on FHIR observation resource builds Measurement information for PHMR.
     *  Supports building Measurement information from FHIR Observation resource that has a ValueQuantity or
     *  has components that have a ValueQuantity. ValueQuantity is required to use UCUM units.
     *  Only supports FHIR observations with effectiveDateTime (not with any of the other effect[x] types).
     *
     * @param observation Input FHIR Observation
     * @return List of Measurements built from input FHIR Observation. If the observation contains components the returned list will contain one entry
     * per component, if the observation does not contain components the returned list will contain one entry. Returns null if observation does not contain ValueQuantity
     * or ValueQuantity is not in UCUM units. Returns null if observation does not have effectiveDateTime.
     * @throws FHIRException if observation.value is not a Quantity
     */
    public List<Measurement> buildMeasurements(Observation observation) throws FHIRException {

        logger.debug("Building CDA Measurement from FHIR Observation: "
                + fhirContext.newJsonParser().setPrettyPrint(true)
                .encodeResourceToString(observation));

        if (!observation.hasValueQuantity() && !observation.hasComponent() && !observation.hasEffectiveDateTimeType())
            return null;

        //We only support observations with ValueQuantity or with components with ValueQuantity.
        //Units must be specified in UCUM.
        if (observation.hasValueQuantity() && !observation.getValueQuantity().getSystem().equals(ServiceVariables.UCUM_SYSTEM.getValue()))
            return null;
        else {
            for (ObservationComponentComponent component: observation.getComponent()) {
                if (!component.hasValueQuantity())
                    return null;
                if (!component.getValueQuantity().getSystem().equals(ServiceVariables.UCUM_SYSTEM.getValue()))
                    return null;
            }
        }

        Measurement.Status measurementStatus;
        if (observation.getStatus() == ObservationStatus.FINAL
                || observation.getStatus() == ObservationStatus.AMENDED
                || observation.getStatus() == ObservationStatus.CORRECTED) {
            measurementStatus = Measurement.Status.COMPLETED;
        }
        else if (observation.getStatus() == Observation.ObservationStatus.CANCELLED || observation.getStatus() == Observation.ObservationStatus.ENTEREDINERROR) {
            measurementStatus = Measurement.Status.NULLIFIED;
        }
        else
            return null; //Don't convert observations w. status REGISTERED/PRELIMINARY/UNKNOWN

        DataInputContext context = buildDataInputContext(observation);
        Date effectiveDateTime = observation.getEffectiveDateTimeType().getValue();

        List<Measurement> measurements = new ArrayList<>();
        if (!observation.hasComponent()) {
            CodeableConcept code = observation.getCode();
            Quantity quantity = observation.getValueQuantity();
            Measurement measurement = buildMeasurement(code, quantity, context, effectiveDateTime, measurementStatus);
            if (measurement == null)
                return null;
            measurements.add(measurement);
        }
        else {
            for (ObservationComponentComponent component: observation.getComponent()) {
                CodeableConcept code = component.getCode();
                Quantity quantity = component.getValueQuantity();
                Measurement measurement = buildMeasurement(code, quantity, context, effectiveDateTime, measurementStatus);
                if (measurement == null)
                    return null;
                measurements.add(measurement);
            }
        }

        return measurements;
    }

    private Measurement buildMeasurement(CodeableConcept code, Quantity quantity, DataInputContext context, Date effectiveDateTime, Measurement.Status measurementStatus) {
        //CONF-PHMR-DK-33:
        ID measurementId = new ID.IDBuilder()
                .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
                .setExtension(UUID.randomUUID().toString())
                .setRoot(MedCom.ROOT_OID).build();

        // code
        String MDCSystem = ServiceVariables.MDC_OID.getValue();
        String alternateMDCSystem = ServiceVariables.MDC_URN.getValue();
        String MDCCode = null;
        String MDCDisplayName = null;
        for(Coding coding: code.getCoding()) {
            if (coding.getSystem().contains(MDCSystem) || coding.getSystem().contains(alternateMDCSystem)) {
                MDCCode = coding.getCode();
                MDCDisplayName = coding.getDisplay();
            }
        }

        if (MDCCode == null)
            return null; //We require MDC

        Measurement.MeasurementBuilder measurementBuilder = new Measurement.MeasurementBuilder(
                effectiveDateTime, measurementStatus)
                .setContext(context);

        measurementBuilder.setPhysicalQuantity(
                quantity.getValue().toString(),
                quantity.getCode(),
                MDCCode,
                MDCDisplayName,
                MDCSystem,
                "MDC"
        );

        measurementBuilder.setId(measurementId);
        Measurement measurement = measurementBuilder.build();

        return measurement;
    }

    private DataInputContext buildDataInputContext(Observation observation) {
        PerformerType performerType = null;
        String fhirPerformerType = "Patient";
        if (observation.hasPerformer() && observation.getPerformerFirstRep().hasIdentifier())
            fhirPerformerType = observation.getPerformerFirstRep().getType();

        if (fhirPerformerType.equals("Patient"))
            performerType = PerformerType.Citizen;
        else if (fhirPerformerType.equals("Practitioner") || fhirPerformerType.equals("PractitionerRole"))
            performerType = PerformerType.HealthcareProfessional; //We have no way of saying if it's a caregiver ("anden omsorgsperson")
        else
            logger.warn("Observation performer type \"" + fhirPerformerType + "\" is not supported");


        ProvisionMethod provisionMethod = null;
        Extension extension = observation.getExtensionByUrl(ServiceVariables.OBSERVATION_DATA_ENTRY_METHOD_EXTENSION_URL.getValue());
        if (extension != null) {
            String code = ((Coding)extension.getValue()).getCode();
            switch (code) {
                case "manual":
                    provisionMethod = ProvisionMethod.TypedByCitizen;

                    if (fhirPerformerType.equals("Patient"))
                        provisionMethod = ProvisionMethod.TypedByCitizen;
                    if (fhirPerformerType.equals("Practitioner") || fhirPerformerType.equals("PractitionerRole"))
                        provisionMethod = ProvisionMethod.TypedByHealthcareProfessional;
                    if (fhirPerformerType.equals("RelatedPerson"))
                        provisionMethod = ProvisionMethod.TypedByCitizenRelative;
                    break;
                case "automatic":
                    provisionMethod = ProvisionMethod.Electronically;
                    break;
                default:
                    logger.warn("Observation data entry method \"" + code + "\" is not supported");
            }
        }

        return new DataInputContext(provisionMethod, performerType);
    }

    /**
     *Based on FHIR device resource builds MedicalEquipment information for PHMR
     *
     * @param device FHIR Device resource
     * @return MedicalEquipment information
     * @throws Exception If FHIR Device does not have required fields
     */
    public MedicalEquipment buildMedicalEquipment(Device device) throws Exception {
        logger.debug("Building CDA MedicalEquipment from FHIR DeviceComponent: "
                + fhirContext.newJsonParser().setPrettyPrint(true)
                .encodeResourceToString(device));

        MedicalEquipment medicalEquipment;
        MedicalEquipmentBuilder equipmentBuilder = new MedicalEquipment.MedicalEquipmentBuilder();

        String medcomCode = (String) MedComInstrumentCodes.map.get(device.getManufacturer(), device.getModelNumber());
        if (medcomCode != null)
            equipmentBuilder.setMedComMedicalDeviceCode(medcomCode);

        String system = null;
        String code = null;
        String display = null;
        if (device.hasSpecialization() && device.getSpecializationFirstRep().hasSystemType()
                && device.getSpecializationFirstRep().getSystemType().hasCoding()) {
            system = device.getSpecializationFirstRep().getSystemType().getCodingFirstRep().getSystem();
            code = device.getSpecializationFirstRep().getSystemType().getCodingFirstRep().getCode();
            display = device.getSpecializationFirstRep().getSystemType().getCodingFirstRep().getDisplay();
        }
        else if (device.hasType() && device.getType().hasCoding()) {
            system = device.getType().getCodingFirstRep().getSystem();
            code = device.getType().getCodingFirstRep().getCode();
            display = device.getType().getCodingFirstRep().getDisplay();
        }
        if (system != null && system.equals(ServiceVariables.MDC_URN.getValue()))
            system = ServiceVariables.MDC_OID.getValue(); //prefer OID for MDC, rather than urn
        if (system != null && code != null)
            equipmentBuilder.setMedicalDeviceCode(system, code, display);

        equipmentBuilder.setMedicalDeviceDisplayName(device.getManufacturer() + " " + device.getModelNumber());
        equipmentBuilder.setManufacturerModelName(device.getModelNumber())
                .setSerialNumber(device.getSerialNumber());

        if (device.hasVersion()) {
            String softwareName = "";
            for (DeviceVersionComponent versionComponent : device.getVersion()) {
                softwareName = softwareName + versionComponent.getValue() + ", ";
            }
            equipmentBuilder.setSoftwareName(softwareName);
        }

        return equipmentBuilder.build();
    }

    /**
     * Based on FHIR Address resource builds AddressData information for QRD
     *
     * @param address
     *            FHIR Address resource
     * @return AddressData
     */
    public AddressData buildAddressData(Address address) {

        AddressData addressData;

        AddressData.AddressBuilder addressBuilder = new AddressData.AddressBuilder(
                address.getPostalCode(), address.getCity());
        for (StringType addressLine : address.getLine()) {
            addressBuilder.addAddressLine(addressLine.getValue());
        }
        addressData = addressBuilder.setCountry(address.getCountry())
                .setUse(AddressData.Use.WorkPlace)
                .build();

        return addressData;
    }

    /**
     * Based on list of FHIR ContactPoint resources builds List of Telecom information for QRD
     t
     * @param contactPoints
     *            List of FHIR ContactPoint resources
     * @return List<Telecom>
     */
    public List<Telecom> buildTelecomList(List<ContactPoint> contactPoints) {

        ArrayList<Telecom> telecomList = new ArrayList<>();

        for (ContactPoint contactPoint: contactPoints) {
            String protocol = null;
            AddressData.Use use;
            String value;

            switch (contactPoint.getSystem()) {
                case PHONE:
                    protocol = "tel";
                    break;
                case EMAIL:
                    protocol = "mailto";
                    break;
                case FAX: case PAGER: case URL: case SMS: case OTHER:
                    protocol = null;
                    break;
            }

            switch (contactPoint.getUse()) {
                case HOME:
                    use = AddressData.Use.HomeAddress;
                    break;
                case WORK:
                    use = AddressData.Use.WorkPlace;
                    break;
                default:
                    use = AddressData.Use.HomeAddress;
                    break;
            }

            value = contactPoint.getValue();

            telecomList.add(
                    new Telecom(use, protocol, value)
            );
        }

        return telecomList;
    }
}
