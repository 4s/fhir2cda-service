package dk.s4.microservices.fhir2cda.convert;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.core.BaseClinicalDocument;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.microservices.fhir2cda.service.ServiceVariables;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Extension;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Practitioner;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.Type;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class for converting FHIR observation resources to HL7 PHMR format
 * resource.
 */
public class Fhir2phmrConverter extends Fhir2cdaConverter<Observation> {
	private final static Logger logger = LoggerFactory.getLogger(Fhir2phmrConverter.class);

	public Fhir2phmrConverter(Codec codec, FhirContext fhirContext, FHIRRestUtils restUtils, FHIR2CDAUtils cdaUtils) {
		super(codec, fhirContext, restUtils, cdaUtils);
	}

	/**
	 * Build PHMR header from FHIR Observation resource
	 * @param fhirResource FHIR Observation
	 * @return PHMR document with header filled
	 * @throws FHIRException if observation.effective is not a DateTimeType.
	 * @throws Exception if observation.subject can not be looked up or if patient cannot be built by FHIR2CDAUtils
	 */
	@Override
	protected BaseClinicalDocument buildHeader(Observation fhirResource) throws Exception {
		Date documentCreationTime = new Date();
		PHMRDocument phmr = initPhmrDocument(fhirResource, documentCreationTime);
		setDocumentationTimeInterval(fhirResource, phmr);

		setPatientAndTitle(phmr, fhirResource);
		setAuthor(phmr, fhirResource, documentCreationTime);
		setCustodian(phmr);
		setLegalAuthenticator(phmr, documentCreationTime);

		return phmr;
	}

	@Override
	protected void buildBody(BaseClinicalDocument cda, Observation observation) throws Exception {
		PHMRDocument phmr = (PHMRDocument)cda;
		//Measurement
		List<Measurement> measurements = cdaUtils.buildMeasurements(observation);
		if (measurements == null || measurements.isEmpty())
			return;

		for (Measurement measurement: measurements) {
			// Add measurement as vital sign or non-vital/ordinary result
			switch (measurement.getCode()) {
				case "150016":
				case "150017":
				case "150018":
				case "150019":
				case "150020":
				case "150021":
				case "150022":
				case "150023":
					phmr.addVitalSign(measurement); // Blood pressure
					break;
				case "150364":
				case "150392":
				case "188420":
				case "188424":
				case "188428":
				case "188432":
				case "188448":
					phmr.addVitalSign(measurement); // Core Body Temperature
					break;
				case "150320":
				case "150456":
					phmr.addVitalSign(measurement); // O2 saturation
					break;
				case "151562":
					phmr.addVitalSign(measurement); // Respiration Rate
					break;
				case "149514":
				case "149546":
				case "149530":
					phmr.addVitalSign(measurement); // Pulse
					break;
				default:
					phmr.addResult(measurement);
					break;
			}
		}

		// Medical Equipment
		Reference deviceReference = observation.getDevice();
		if (deviceReference != null && !deviceReference.isEmpty() && deviceReference.hasIdentifier()) {
			Device device = restUtils.getDevice(deviceReference);
			if (device != null) {
				MedicalEquipment medicalEquipment = cdaUtils.buildMedicalEquipment(device);
				phmr.addMedicalEquipment(medicalEquipment);
			}
		}

		//Personal Health Gateway device
		Extension phgExtension = observation.getExtensionByUrl(ServiceVariables.PHG_OBSERVATION_EXTENSION_URL.getValue());
		if (phgExtension != null && phgExtension.hasValue()) {
			Type value = phgExtension.getValue();
			if (value instanceof Reference) {
				Device phgDevice = restUtils.getDevice((Reference)value);
				if (phgDevice != null) {
					MedicalEquipment medicalEquipment = cdaUtils.buildMedicalEquipment(phgDevice);
					phmr.addMedicalEquipment(medicalEquipment);
				}
			}
		}
	}

	private void setAuthor(PHMRDocument phmr, Observation observation, Date documentCreationTime) throws Exception {
		if (!observation.hasBasedOn())
			throw new Exception("Observation is missing basedOn");
		Organization organization = getBasedOnOrganization(observation.getBasedOn());
		OrganizationIdentity authorOrganisationIdentity = cdaUtils.buildOrganizationIdentity(organization);
		Participant participant = cdaUtils.buildParticipant(authorOrganisationIdentity, null, documentCreationTime);
		phmr.setAuthor(participant);
	}

	private void setLegalAuthenticator(PHMRDocument phmr, Date documentCreationTime) {
		String legalAuthOrgString = ServiceVariables.LEGAL_AUTHENTICATOR_ORGANIZATOIN.getValue();
		Organization legalAuthOrganization = fhirContext.newJsonParser().parseResource(Organization.class, legalAuthOrgString);
		OrganizationIdentity legalAuthenticatorOrganizationIdentity = cdaUtils.buildOrganizationIdentity(legalAuthOrganization);

		PersonIdentity legalAuthenticatorPersonIdentity = null;
		if (ServiceVariables.LEGAL_AUTHENTICATOR_PRACTITIONER.getValue() != null) {
			String legalAuthPractitionerString = ServiceVariables.LEGAL_AUTHENTICATOR_PRACTITIONER.getValue();
			Practitioner legalAuthenticatorPractitioner = fhirContext.newJsonParser().parseResource(Practitioner.class, legalAuthPractitionerString);
			legalAuthenticatorPersonIdentity = cdaUtils.buildPersonIdentity(legalAuthenticatorPractitioner);
		}

		Participant participant = cdaUtils.buildParticipant(legalAuthenticatorOrganizationIdentity, legalAuthenticatorPersonIdentity, documentCreationTime);
		phmr.setLegalAuthenticator(participant);
	}

	private void setDocumentationTimeInterval(Observation observation, PHMRDocument phmr) throws Exception {
		Date startTime, endTime;
		if (observation.hasEffectivePeriod()) {
			startTime = observation.getEffectivePeriod().getStart();
			endTime = observation.getEffectivePeriod().getEnd();
		}
		else if (observation.hasEffectiveDateTimeType()) {
			startTime = observation.getEffectiveDateTimeType().getValue();
			endTime = observation.getEffectiveDateTimeType().getValue();
		}
		else
			throw new Exception("FHIR Observation must have either EffectiveDateTime or EffectivePeriod");

		// documentationOf
		phmr.setDocumentationTimeInterval(startTime, endTime);
	}

	private PHMRDocument initPhmrDocument(Observation observation, Date documentCreationTime) {
		// Use an observation identifier as PHMR id. Either one in the MedCom OID or one in the official obs.identifier system.
		// If none of these can be found use a randow UUID.
		String documentId = "";
		for (Identifier identifier : observation.getIdentifier()) {
			if (identifier.hasSystem()) {
				if (identifier.getSystem().equals(ServiceVariables.MEDCOM_OID.getValue())) {
					documentId = identifier.getValue();
					break;
				}
				if (identifier.getSystem().equals(ServiceVariables.OFFICIAL_OBSERVATION_IDENTIFIER_SYSTEM.getValue())) {
					documentId = identifier.getValue();
				}
			}
		}
		if (documentId != null && documentId.isEmpty())
			documentId = UUID.randomUUID().toString();

		PHMRDocument phmr = new PHMRDocument(new ID.IDBuilder()
				.setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
				.setExtension(documentId)
				.setRoot(MedCom.ROOT_OID).build()
		);

		// Time when this document is created. Not based on values from FHIR resources.
		phmr.setEffectiveTime(documentCreationTime);
		phmr.setLanguageCode("da-DK");

		return phmr;
	}


	private void setPatientAndTitle(PHMRDocument phmr, Observation observation) throws Exception {
		if (!observation.hasSubject() || !observation.getSubject().hasIdentifier())
			throw new Exception("Observation is missing subject");

		org.hl7.fhir.r4.model.Patient patient = restUtils.getPatient(observation.getSubject().getIdentifier());
		if (patient == null || patient.isEmpty())
			throw new Exception("Patient is null or empty");

		Patient cdaPatient = cdaUtils.buildPatient(patient);
		if (cdaPatient == null)
			throw new Exception("Building CDA patient failed");

		phmr.setTitle("Hjemmemonitorering for " + cdaPatient.getIdValue());
		phmr.setPatient(cdaPatient);
	}
}
