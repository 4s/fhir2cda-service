package dk.s4.microservices.fhir2cda.service;

import java.util.regex.Pattern;
import junit.framework.TestCase;
import org.junit.ClassRule;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

public class ServiceTopicsTest extends TestCase {

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    public void testGetNoEnv() {
        Pattern pattern = ServiceTopics.get();
        String topicPatternString = pattern.toString();
        assertEquals("DataCreated_FHIR_Observation(_.*)*|DataUpdated_FHIR_Observation(_.*)*|DataCreated_FHIR_Questionnaire|DataUpdated_FHIR_Questionnaire", topicPatternString);
    }

    public void testGetEnv() {
        environmentVariables.set("SERVICE_INPUT_TOPIC_PATTERN_STRING", "testpattern*");
        Pattern pattern = ServiceTopics.get();
        String topicPatternString = pattern.toString();
        assertEquals("testpattern*", topicPatternString);
    }

}