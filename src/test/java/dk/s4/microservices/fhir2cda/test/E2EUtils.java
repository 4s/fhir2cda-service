package dk.s4.microservices.fhir2cda.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;


import ca.uhn.fhir.context.FhirContext;
import com.google.common.io.Resources;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.microservices.fhir2cda.convert.FHIR2CDAUtils;
import dk.s4.microservices.fhir2cda.convert.FHIRRestUtils;
import dk.s4.microservices.fhir2cda.convert.Fhir2phmrConverter;
import dk.s4.microservices.fhir2cda.convert.Fhir2qrdConverter;
import dk.s4.microservices.fhir2cda.convert.FhirContextUtil;
import dk.s4.microservices.fhir2cda.kafka.MyEventProcessor;
import dk.s4.microservices.fhir2cda.kafka.TopicFilterInterceptor;
import dk.s4.microservices.fhir2cda.service.Fhir2cda;
import dk.s4.microservices.fhir2cda.service.ServiceTopics;
import dk.s4.microservices.fhir2cda.service.ServiceVariables;
import dk.s4.microservices.messaging.EventProcessor;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.MessagingInitializationException;
import dk.s4.microservices.messaging.MessagingUtils;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.Topic.Category;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.kafka.KafkaInitializationException;
import dk.s4.microservices.messaging.mock.MockKafkaConsumeAndProcess;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.Serializer;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.ContactPoint;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointUse;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.StringType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class E2EUtils {
    private final static Logger logger = LoggerFactory.getLogger(E2EUtils.class);

    public static final String GENERATED_PATH =  "docs/generated-by-fhir2cda/";

    public static MockProducer<String, String> initMockExternalProducer()
            throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        //Read KafkaProducer properties and init producer from these
        InputStream props = Resources.getResource("producer.props").openStream();
        Properties properties = new Properties();
        properties.load(props);
        Serializer<String> keySerializer = null;
        Serializer<String> valueSerializer = null;

        // Instantiate Kafka Key- and Value-serializers from strings
        keySerializer =
                (Serializer<String>) Class.forName(properties.getProperty("key.serializer")).newInstance();
        valueSerializer =
                (Serializer<String>)Class.forName(properties.getProperty("value.serializer")).newInstance();
        return new MockProducer<>(true, keySerializer, valueSerializer);
    }

    public static MockKafkaEventProducer initMockKafkaConsumeAndProcess(FHIRRestUtils restUtils)
            throws KafkaInitializationException, MessagingInitializationException {
        FhirContext fhirContext = FhirContextUtil.getFhirContext();
        Fhir2phmrConverter phmrConverter = new Fhir2phmrConverter(new PHMRXmlCodec(), fhirContext, restUtils, new FHIR2CDAUtils(fhirContext));
        Fhir2qrdConverter qrdConverter = new Fhir2qrdConverter(new QRDXmlCodec(), fhirContext, restUtils, new FHIR2CDAUtils(fhirContext));

        EventProcessor eventProcessor = new MyEventProcessor(FhirContextUtil.getFhirContext(), phmrConverter, qrdConverter);
        MockKafkaEventProducer mockKafkaEventProducer = new MockKafkaEventProducer(System.getenv("SERVICE_NAME"));

        MockKafkaConsumeAndProcess kafkaConsumeAndProcess = new MockKafkaConsumeAndProcess(
                ServiceTopics.get(),
                /*Arrays.asList(new Topic().setOperation(Operation.DataCreated).setDataCategory(Category.FHIR).setDataType("Observation").setDataCodes(Arrays.asList("MDC188736")))*/
                /*topicPattern*/
                mockKafkaEventProducer,
                eventProcessor);
        kafkaConsumeAndProcess.registerInterceptor(new TopicFilterInterceptor());
        Fhir2cda.kafkaConsumeAndProcessThread = new Thread(kafkaConsumeAndProcess);

        return mockKafkaEventProducer;
    }

    public static Message sendAndReceiveMessage(
            MockProducer<String, String> mockExternalKafkaProducer,
            MockKafkaEventProducer mockKafkaEventProducer,
            String fhirResourceAsString,
            String dataCode,
            String inputDataType,
            String outputDataType,
            Operation outputOperation) throws Exception {
        Message kafkaMessage = new Message()
                .setBody(fhirResourceAsString)
                .setSender("FHIR2CDAServiceTest")
                .setCorrelationId(MessagingUtils.verifyOrCreateId(null))
                .setTransactionId(UUID.randomUUID().toString())
                .setBodyType("FHIR")
                .setContentVersion("R4");

        ArrayList<String> dataCodes = new ArrayList<>();
        dataCodes.add(dataCode);
        Topic inputTopic = new Topic()
                .setOperation(Operation.DataCreated)
                .setDataCategory(Category.FHIR)
                .setDataType(inputDataType);
        if (dataCode != null)
            inputTopic.setDataCodes(dataCodes);

        logger.debug(kafkaMessage.toString());
        //Send resource string via KafkaProducer, and wait 5000 ms for Future to return
        ProducerRecord<String, String> record = new ProducerRecord<String, String>(inputTopic.toString(), kafkaMessage.toString());
        RecordMetadata metadata = mockExternalKafkaProducer.send(record).get(5000, TimeUnit.MILLISECONDS);
        assertNotNull(metadata);

        Topic outputTopic = new Topic()
                .setOperation(outputOperation)
                .setDataCategory(Topic.Category.CDA)
                .setDataType(outputDataType);
        if (dataCode != null)
            outputTopic.setDataCodes(dataCodes);
        Future<Message> future = mockKafkaEventProducer.getTopicFuture(outputTopic);;
        Message outputMessage = future.get(500000, TimeUnit.MILLISECONDS);

        assertEquals(kafkaMessage.getCorrelationId(), outputMessage.getCorrelationId());
        assertEquals(kafkaMessage.getTransactionId(), outputMessage.getTransactionId());

        return outputMessage;
    }

    public static void setupRestUtils(FHIRRestUtils restUtils) throws Exception {
        Patient patient = new Patient();
        patient.addName().addGiven("Nancy Ann Test").setFamily("Berggren");
        patient.addIdentifier().setSystem("urn:oid:1.2.208.176.1.2").setValue("2512489996");
        patient.setId("http://example.com/patient_service/1");

        String deviceAsString = ResourceUtil.stringFromResource("FHIR-clean/Individual-resources/Device_WeightScale.json");
        Device device = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Device.class, deviceAsString);
        device.setManufacturer("A&D Company"); //This is the manufacturer name in the official MedCom instrument code table for this device

        CarePlan carePlan = new CarePlan().setAuthor(new Reference().setIdentifier(new Identifier()
                .setSystem("careplan-test-system")
                .setValue("careplan-test-value")).setType("CarePlan"));

        Identifier orgIdentifier = new Identifier().setSystem(ServiceVariables.ORGANIZATION_IDENTIFIER_SYSTEM.getValue()).setValue("310541000016007");
        Organization organization = new Organization().setIdentifier(Arrays.asList(orgIdentifier));
        organization.setName("Test organization");
        organization.addAddress()
                .setPostalCode("8000")
                .setCity("Aarhus")
                .setLine(Arrays.asList(new StringType("Aabogade 34")))
                .setCountry("Danmark");
        organization.addTelecom(new ContactPoint().setSystem(ContactPointSystem.PHONE).setUse(ContactPointUse.HOME).setValue("+45123456"));

        when(restUtils.getOrganization(any())).thenReturn(organization);
        when(restUtils.getPatient(any())).thenReturn(patient);
        when(restUtils.getDevice(any())).thenReturn(device);
        when(restUtils.getCarePlan(any())).thenReturn(carePlan);
    }

    public static void writeOutputToFile(String output, String filename) throws IOException {
        logger.debug("writeOutputToFile");
        if (ServiceVariables.TEST_WRITE_XML_TO_FILE.isSetToTrue()) {
            logger.debug("ServiceVariables.TEST_WRITE_XML_TO_FILE.isSetToTrue");
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
            writer.write(output);
            writer.close();
        }
    }
}
