package dk.s4.microservices.fhir2cda.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;


import dk.s4.microservices.fhir2cda.convert.FHIRRestUtils;
import dk.s4.microservices.fhir2cda.service.Fhir2cda;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class E2EWeightTest {

    private final static Logger logger = LoggerFactory
            .getLogger(E2EWeightTest.class);

    private static FHIRRestUtils restUtils;
    private static String fhirResourceAsString;

    private static MockProducer<String, String> mockExternalKafkaProducer;
    private static MockKafkaEventProducer mockKafkaEventProducer;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();


    @Before
    public void before() throws Exception {
        TestUtils.readEnvironment("service.env", environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);

        restUtils = mock(FHIRRestUtils.class);
        E2EUtils.setupRestUtils(restUtils);

        mockKafkaEventProducer = E2EUtils.initMockKafkaConsumeAndProcess(restUtils);
        mockExternalKafkaProducer = E2EUtils.initMockExternalProducer();

        fhirResourceAsString = ResourceUtil.stringFromResource("Observation_weight.json");

        logger.info("Starting Fhir2cda.main");
        Fhir2cda.main(new String[] {});
    }

    @After
    public void after() throws Exception {

    }

    @Test
    public void testConvertWeightObsViaKafka() throws Exception {
        mockKafkaEventProducer.resetHistory();

        Message outputMessage = E2EUtils.sendAndReceiveMessage(
                mockExternalKafkaProducer,
                mockKafkaEventProducer,
                fhirResourceAsString,
                "MDC188736",
                "Observation",
                "PHMR",
                Topic.Operation.DataConverted);

        assertTrue(outputMessage.getBody().contains("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"));
        assertTrue(outputMessage.getBody().contains("</ClinicalDocument>"));

        E2EUtils.writeOutputToFile(outputMessage.getBody(), E2EUtils.GENERATED_PATH + "phmr_weight.xml");
    }
}
