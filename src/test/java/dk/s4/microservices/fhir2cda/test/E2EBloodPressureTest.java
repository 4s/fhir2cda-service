package dk.s4.microservices.fhir2cda.test;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import dk.s4.microservices.fhir2cda.convert.FHIRRestUtils;
import dk.s4.microservices.fhir2cda.convert.FhirContextUtil;
import dk.s4.microservices.fhir2cda.service.Fhir2cda;
import dk.s4.microservices.messaging.Message;
import dk.s4.microservices.messaging.Topic.Operation;
import dk.s4.microservices.messaging.mock.MockKafkaEventProducer;
import dk.s4.microservices.messaging.mock.MockProducer;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Reference;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.ArgumentMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class E2EBloodPressureTest {

    private final static Logger logger = LoggerFactory
            .getLogger(E2EBloodPressureTest.class);

    private static FHIRRestUtils restUtils;
    private static String fhirResourceAsString;

    private static MockProducer<String, String> mockExternalKafkaProducer;
    private static MockKafkaEventProducer mockKafkaEventProducer;

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();


    @Before
    public void before() throws Exception {
        TestUtils.readEnvironment("service.env", environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);

        restUtils = mock(FHIRRestUtils.class);
        E2EUtils.setupRestUtils(restUtils);

        mockKafkaEventProducer = E2EUtils.initMockKafkaConsumeAndProcess(restUtils);
        mockExternalKafkaProducer = E2EUtils.initMockExternalProducer();

        fhirResourceAsString = ResourceUtil.stringFromResource("Observation_bloodpressure.json");

        logger.info("Starting Fhir2cda.main");
        Fhir2cda.main(new String[] {});
    }

    @After
    public void after() throws Exception {

    }

    private class PhgDeviceReferenceMatcher extends ArgumentMatcher<Reference> {
        @Override
        public boolean matches(Object o) {
            if (o instanceof Reference) {
                Reference reference = (Reference)o;
                if (reference.getIdentifier().getValue().equals("C4-B3-01-D2-80-C6"))
                    return true;
            }
            return false;
        }
    }

    private class BloodPressureDeviceReferenceMatcher extends ArgumentMatcher<Reference> {
        @Override
        public boolean matches(Object o) {
            if (o instanceof Reference) {
                Reference reference = (Reference)o;
                if (reference.getIdentifier().getValue().equals("00-09-1F-FE-FF-80-1A-33"))
                    return true;
            }
            return false;
        }
    }

    @Test
    public void testConvertBloodPressureObsViaKafka() throws Exception {
        String deviceAsString = ResourceUtil.stringFromResource("FHIR-clean/Individual-resources/Device_BloodPressure.json");
        Device device = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Device.class, deviceAsString);
        when(restUtils.getDevice(argThat(new BloodPressureDeviceReferenceMatcher()))).thenReturn(device);

        String phgDeviceAsString = ResourceUtil.stringFromResource("FHIR-clean/Individual-resources/Device_PHG.json");
        Device phgDevice = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Device.class, phgDeviceAsString);
        when(restUtils.getDevice(argThat(new PhgDeviceReferenceMatcher()))).thenReturn(phgDevice);

        Message outputMessage = E2EUtils.sendAndReceiveMessage(
                mockExternalKafkaProducer,
                mockKafkaEventProducer,
                fhirResourceAsString,"MDC150020", "Observation", "PHMR", Operation.DataConverted);

        assertTrue(outputMessage.getBody().contains("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>"));
        assertTrue(outputMessage.getBody().contains("</ClinicalDocument>"));

        E2EUtils.writeOutputToFile(outputMessage.getBody(), E2EUtils.GENERATED_PATH + "phmr_bloodpressure.xml");
    }
}
