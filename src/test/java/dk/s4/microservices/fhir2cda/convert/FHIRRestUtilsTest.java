package dk.s4.microservices.fhir2cda.convert;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.okJson;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlPathMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import dk.s4.microservices.microservicecommon.TestUtils;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Questionnaire;
import org.hl7.fhir.r4.model.Reference;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

public class FHIRRestUtilsTest {
    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(wireMockConfig().dynamicPort());

    private static String serviceUrl;

    private static final String DEVICE_MATCHED_BUNDLE = "{\n"
            + "  \"resourceType\": \"Bundle\",\n"
            + "  \"id\": \"33dcb6bd-8cef-4bd5-8282-091132146337\",\n"
            + "  \"meta\": {\n"
            + "    \"lastUpdated\": \"2019-10-23T08:02:52.039+00:00\"\n"
            + "  },\n"
            + "  \"type\": \"searchset\",\n"
            + "  \"link\": [\n"
            + "    {\n"
            + "      \"relation\": \"self\",\n"
            + "      \"url\": \"http://example.com/baseR4/Device?_format=json&_pretty=true\"\n"
            + "    },\n"
            + "    {\n"
            + "      \"relation\": \"next\",\n"
            + "      \"url\": \"http://example.com/baseR4?_getpages=33dcb6bd-8cef-4bd5-8282-091132146337&_getpagesoffset=20&_count=20&_format=json&_pretty=true&_bundletype=searchset\"\n"
            + "    }\n"
            + "  ],\n"
            + "  \"entry\": [\n"
            + "    {\n"
            + "      \"fullUrl\": \"http://example.com/baseR4/Device/7\",\n"
            + "      \"resource\": \n"
            + "      {\n"
            + "        \"resourceType\": \"Device\",\n"
            + "        \"id\": \"008098FEFF0E3913.0080980E3913\",\n"
            + "        \"meta\": {\n"
            + "          \"profile\": [\n"
            + "            \"http://hl7.org/fhir/uv/phd/StructureDefinition/PhdDevice\"\n"
            + "          ]\n"
            + "        },\n"
            + "        \"identifier\": [\n"
            + "          {\n"
            + "            \"type\": {\n"
            + "              \"coding\": [{\n"
            + "                \"system\": \"http://hl7.org/fhir/uv/phd/CodeSystem/ContinuaDeviceIdentifiers\",\n"
            + "                \"code\": \"SYSID\"\n"
            + "              }]\n"
            + "            },\n"
            + "            \"system\": \"urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680\",\n"
            + "            \"value\": \"00-80-98-FE-FF-0E-39-13\"\n"
            + "          }\n"
            + "        ],\n"
            + "        \"type\": {\n"
            + "          \"coding\": [\n"
            + "            {\n"
            + "              \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "              \"code\": \"65573\",\n"
            + "              \"display\": \"MDC_MOC_VMS_MDS_SIMP\"\n"
            + "            }\n"
            + "          ],\n"
            + "          \"text\": \"MDC_MOC_VMS_MDS_SIMP: Continua Personal Health Device\"\n"
            + "        },\n"
            + "        \"specialization\": [\n"
            + "          {\n"
            + "            \"systemType\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "                  \"code\": \"585728\",\n"
            + "                  \"display\": \"MDC_DEV_PLACEHOLDER_SPEC_PROFILE_CTG\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"MDC_DEV_PLACEHOLDER_SPEC_PROFILE_CTG: CTG recorder\"\n"
            + "            },\n"
            + "            \"version\": \"0\"\n"
            + "          }\n"
            + "        ],\n"
            + "        \"manufacturer\": \"Monica Healthcare\",\n"
            + "        \"modelNumber\": \"AN24V1\",\n"
            + "        \"serialNumber\": \"A001286\",\n"
            + "        \"version\": [\n"
            + "          {\n"
            + "            \"type\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "                  \"code\": \"531975\",\n"
            + "                  \"display\": \"MDC_ID_PROD_SPEC_SW\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"Software revision\"\n"
            + "            },\n"
            + "            \"value\": \"A.02.00\"\n"
            + "          },{\n"
            + "            \"type\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "                  \"code\": \"531976\",\n"
            + "                  \"display\": \"MDC_ID_PROD_SPEC_FW\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"Firmware revision\"\n"
            + "            },\n"
            + "            \"value\": \"6.520\"\n"
            + "          },{\n"
            + "            \"type\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "                  \"code\": \"531977\",\n"
            + "                  \"display\": \"MDC_ID_PROD_SPEC_PROTOCOL\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"Protocol revision\"\n"
            + "            },\n"
            + "            \"value\": \"A.03.00\"\n"
            + "          },{\n"
            + "            \"type\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"urn:iso:std:iso:11073:10101\",\n"
            + "                  \"code\": \"532352\",\n"
            + "                  \"display\": \"MDC_REG_CERT_DATA_CONTINUA_VERSION\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"Continua Design Guidelines version\"\n"
            + "            },\n"
            + "            \"value\": \"07.00\"\n"
            + "          }\n"
            + "        ],\n"
            + "        \"property\": [\n"
            + "          {\n"
            + "            \"type\": {\n"
            + "              \"coding\": [\n"
            + "                {\n"
            + "                  \"system\": \"http://hl7.org/fhir/uv/phd/CodeSystem/ASN1ToHL7\",\n"
            + "                  \"code\": \"532354.0\",\n"
            + "                  \"display\": \"regulation-status\"\n"
            + "                }\n"
            + "              ],\n"
            + "              \"text\": \"Regulation status\"\n"
            + "            },\n"
            + "            \"valueCode\": [\n"
            + "              {\n"
            + "                \"coding\": [\n"
            + "                  {\n"
            + "                    \"system\": \"http://terminology.hl7.org/CodeSystem/v2-0136\",\n"
            + "                    \"code\": \"N\",\n"
            + "                    \"display\": \"regulated\"\n"
            + "                  }\n"
            + "                ],\n"
            + "                \"text\": \"Regulated medical device\"\n"
            + "              }\n"
            + "            ]\n"
            + "          }\n"
            + "        ]\n"
            + "      },\n"
            + "      \"search\": {\n"
            + "        \"mode\": \"match\"\n"
            + "      }\n"
            + "    }\n"
            + "  ]\n"
            + "}\n";

    private static final String CAREPLAN_MATCHED_BUNDLE = "{\n"
            + "  \"resourceType\": \"Bundle\",\n"
            + "  \"id\": \"32d58924-964d-4033-ba86-696723459a38\",\n"
            + "  \"meta\": {\n"
            + "    \"lastUpdated\": \"2020-12-08T13:13:43.714+00:00\"\n"
            + "  },\n"
            + "  \"type\": \"searchset\",\n"
            + "  \"link\": [ {\n"
            + "    \"relation\": \"self\",\n"
            + "    \"url\": \"http://hapi.fhir.org/baseR4/CarePlan?_pretty=true\"\n"
            + "  }, {\n"
            + "    \"relation\": \"next\",\n"
            + "    \"url\": \"http://hapi.fhir.org/baseR4?_getpages=32d58924-964d-4033-ba86-696723459a38&_getpagesoffset=20&_count=20&_pretty=true&_bundletype=searchset\"\n"
            + "  } ],\n"
            + "  \"entry\": [ {\n"
            + "    \"fullUrl\": \"http://hapi.fhir.org/baseR4/CarePlan/1686417\",\n"
            + "    \"resource\": {\n"
            + "      \"resourceType\": \"CarePlan\",\n"
            + "      \"id\": \"1234\",\n"
            + "      \"identifier\": [ {\n"
            + "        \"system\": \"careplan-test-system\",\n"
            + "        \"value\": \"careplan-test-value\"\n"
            + "      } ],\n"
            + "      \"status\": \"active\"\n"
            + "    },\n"
            + "    \"search\": {\n"
            + "      \"mode\": \"match\"\n"
            + "    }\n"
            + "  } ]\n"
            + "}";

    private static final String ORGANIZATION_MATCHED_BUNDLE = "{\n"
            + "\t\"resourceType\": \"Bundle\",\n"
            + "\t\"id\": \"0d06a587-822c-400e-bf3b-9d380f6c27d9\",\n"
            + "\t\"meta\": {\n"
            + "\t  \"lastUpdated\": \"2020-12-08T13:28:24.912+00:00\"\n"
            + "\t},\n"
            + "\t\"type\": \"searchset\",\n"
            + "\t\"link\": [ {\n"
            + "\t  \"relation\": \"self\",\n"
            + "\t  \"url\": \"http://hapi.fhir.org/baseR4/Organization?_format=json&_pretty=true\"\n"
            + "\t}, {\n"
            + "\t  \"relation\": \"next\",\n"
            + "\t  \"url\": \"http://hapi.fhir.org/baseR4?_getpages=0d06a587-822c-400e-bf3b-9d380f6c27d9&_getpagesoffset=20&_count=20&_format=json&_pretty=true&_bundletype=searchset\"\n"
            + "\t} ],\n"
            + "\t\"entry\": [ {\n"
            + "\t  \"fullUrl\": \"http://hapi.fhir.org/baseR4/Organization/1704632\",\n"
            + "\t  \"resource\": {\n"
            + "\t\t\"resourceType\": \"Organization\",\n"
            + "\t\t\"id\": \"1234\",\n"
            + "\t\t\"identifier\": [ {\n"
            + "\t\t  \"system\": \"org-test-system\",\n"
            + "\t\t  \"value\": \"org-test-value\"\n"
            + "\t\t} ],\n"
            + "\t\t\"name\": \"testorganisation\"\n"
            + "\t  },\n"
            + "\t  \"search\": {\n"
            + "\t\t\"mode\": \"match\"\n"
            + "\t  }\n"
            + "\t} ]\n"
            + "  }\n"
            + "  ";

    private static final String PATIENT_MATCHED_BUNDLE = "{\n"
            + "\t\"resourceType\": \"Bundle\",\n"
            + "\t\"id\": \"19c4bc59-e6cd-4dae-8e96-6ff8f6881d6b\",\n"
            + "\t\"meta\": {\n"
            + "\t  \"lastUpdated\": \"2020-12-08T13:36:53.667+00:00\"\n"
            + "\t},\n"
            + "\t\"type\": \"searchset\",\n"
            + "\t\"link\": [ {\n"
            + "\t  \"relation\": \"self\",\n"
            + "\t  \"url\": \"http://hapi.fhir.org/baseR4/Patient?_format=json&_pretty=true\"\n"
            + "\t}, {\n"
            + "\t  \"relation\": \"next\",\n"
            + "\t  \"url\": \"http://hapi.fhir.org/baseR4?_getpages=19c4bc59-e6cd-4dae-8e96-6ff8f6881d6b&_getpagesoffset=20&_count=20&_format=json&_pretty=true&_bundletype=searchset\"\n"
            + "\t} ],\n"
            + "\t\"entry\": [ {\n"
            + "\t  \"fullUrl\": \"http://hapi.fhir.org/baseR4/Patient/1217423\",\n"
            + "\t  \"resource\": {\n"
            + "\t\t\"resourceType\": \"Patient\",\n"
            + "\t\t\"id\": \"1234\",\n"
            + "\t\t\"identifier\": {\n"
            + "\t\t  \"system\": \"patient-test-system\",\n"
            + "\t\t  \"value\": \"patient-test-value\"\n"
            + "\t\t}\n"
            + "\t  },\n"
            + "\t  \"search\": {\n"
            + "\t\t\"mode\": \"match\"\n"
            + "\t  }\n"
            + "\t} ]\n"
            + "  }\n"
            + "  ";

    private static final String QUESTIONNAIRE_MATCHED_BUNDLE = "{\n"
            + "\t\"resourceType\": \"Bundle\",\n"
            + "\t\"id\": \"19c4bc59-e6cd-4dae-8e96-6ff8f6881d6b\",\n"
            + "\t\"meta\": {\n"
            + "\t  \"lastUpdated\": \"2020-12-08T13:36:53.667+00:00\"\n"
            + "\t},\n"
            + "\t\"type\": \"searchset\",\n"
            + "\t\"link\": [ {\n"
            + "\t  \"relation\": \"self\",\n"
            + "\t  \"url\": \"http://hapi.fhir.org/baseR4/Questionnaire?_format=json&_pretty=true\"\n"
            + "\t}, {\n"
            + "\t  \"relation\": \"next\",\n"
            + "\t  \"url\": \"http://hapi.fhir.org/baseR4?_getpages=19c4bc59-e6cd-4dae-8e96-6ff8f6881d6b&_getpagesoffset=20&_count=20&_format=json&_pretty=true&_bundletype=searchset\"\n"
            + "\t} ],\n"
            + "\t\"entry\": [ {\n"
            + "\t  \"fullUrl\": \"http://hapi.fhir.org/baseR4/Questionnaire/1217423\",\n"
            + "\t  \"resource\": {\n"
            + "\t\t\"resourceType\": \"Questionnaire\",\n"
            + "\t\t\"id\": \"1234\",\n"
            + "\t\t\"identifier\": {\n"
            + "\t\t  \"system\": \"questionnaire-test-system\",\n"
            + "\t\t  \"value\": \"questionnaire-test-value\"\n"
            + "\t\t}\n"
            + "\t  },\n"
            + "\t  \"search\": {\n"
            + "\t\t\"mode\": \"match\"\n"
            + "\t  }\n"
            + "\t} ]\n"
            + "  }\n"
            + "  ";

    public void testGetDevice() {
    }

    @Before
    public void setUp() throws Exception {

        TestUtils.readEnvironment("deploy.env", environmentVariables);
        TestUtils.readEnvironment("service.env", environmentVariables);

        serviceUrl = "http://localhost:" + wireMockRule.port();
    }

    @Test
    public void getDeviceNoMatchingDevice() throws Exception {

        stubFor(get(urlPathMatching("/Device"))
                .willReturn(okJson("")
                        .withStatus(404)
                        .withHeader("Content-Type", "application/json")
                )
        );

        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Reference deviceReference = new Reference()
                .setType("Device")
                .setIdentifier(new Identifier().setSystem("myTestSystem").setValue("myTestValue"));
        Throwable thrown = catchThrowable(() -> restUtils.getDevice(deviceReference));;

        assertTrue(thrown instanceof ResourceNotFoundException);
        assertTrue(thrown.getMessage().contains("HTTP 404 Not Found"));
    }

    @Test
    public void getDeviceMatchingDevice() throws Exception {
        stubFor(get(urlPathMatching("/Device"))
                .willReturn(okJson(DEVICE_MATCHED_BUNDLE)
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                )
        );

        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Reference deviceReference = new Reference()
                .setType("Device")
                .setIdentifier(new Identifier().setSystem("myTestSystem").setValue("myTestValue"));
        Device device = restUtils.getDevice(deviceReference);

        assertEquals(device.getIdentifier().get(0).getSystem(), "urn:oid:1.2.840.10004.1.1.1.0.0.1.0.0.1.2680");
        assertEquals(device.getIdentifier().get(0).getValue(), "00-80-98-FE-FF-0E-39-13");
    }

    @Test
    public void getCarePlanNoMatch() {
        stubFor(get(urlPathMatching("/CarePlan"))
                .willReturn(okJson("")
                        .withStatus(404)
                        .withHeader("Content-Type", "application/json")
                )
        );

        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Throwable thrown = catchThrowable(() -> restUtils.getCarePlan(new Identifier()));;

        assertTrue(thrown instanceof ResourceNotFoundException);
        assertTrue(thrown.getMessage().contains("HTTP 404 Not Found"));
    }

    @Test
    public void getCarePlanMatch() {
        stubFor(get(urlPathMatching("/CarePlan"))
                .willReturn(okJson(CAREPLAN_MATCHED_BUNDLE)
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                )
        );

        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        CarePlan carePlan = restUtils.getCarePlan(new Identifier());

        assertEquals("careplan-test-system", carePlan.getIdentifier().get(0).getSystem());
        assertEquals("careplan-test-value", carePlan.getIdentifier().get(0).getValue());
    }

    @Test
    public void getOrganizationNoMatch() {
        stubFor(get(urlPathMatching("/Organization"))
                .willReturn(okJson("")
                        .withStatus(404)
                        .withHeader("Content-Type", "application/json")
                )
        );

        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Throwable thrown = catchThrowable(() -> restUtils.getOrganization(new Identifier()));;

        assertTrue(thrown instanceof ResourceNotFoundException);
        assertTrue(thrown.getMessage().contains("HTTP 404 Not Found"));
    }

    @Test
    public void getOrganizationMatch() {
        stubFor(get(urlPathMatching("/Organization"))
                .willReturn(okJson(ORGANIZATION_MATCHED_BUNDLE)
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                )
        );

        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Organization organization = restUtils.getOrganization(new Identifier());

        assertEquals("org-test-system", organization.getIdentifier().get(0).getSystem());
        assertEquals("org-test-value", organization.getIdentifier().get(0).getValue());
    }

    @Test
    public void getPatientNoMatch() {
        stubFor(get(urlPathMatching("/Patient"))
                .willReturn(okJson("")
                        .withStatus(404)
                        .withHeader("Content-Type", "application/json")
                )
        );

        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Throwable thrown = catchThrowable(() -> restUtils.getPatient(new Identifier()));;

        assertTrue(thrown instanceof ResourceNotFoundException);
        assertTrue(thrown.getMessage().contains("HTTP 404 Not Found"));
    }

    @Test
    public void getPatientMatch() throws Exception {
        stubFor(get(urlPathMatching("/Patient"))
                .willReturn(okJson(PATIENT_MATCHED_BUNDLE)
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                )
        );

        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Patient patient = restUtils.getPatient(new Identifier());

        assertEquals("patient-test-system", patient.getIdentifier().get(0).getSystem());
        assertEquals("patient-test-value", patient.getIdentifier().get(0).getValue());
    }

    @Test
    public void getQuestionnaireNoMatch() {
        stubFor(get(urlPathMatching("/Questionnaire"))
                .willReturn(okJson("")
                        .withStatus(404)
                        .withHeader("Content-Type", "application/json")
                )
        );

        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Throwable thrown = catchThrowable(() -> restUtils.getQuestionnaire("canonical://uuid/Questionnaire/2761fbe9-f4d0-49c9-b96e-ae1129d1d4aa|1.0.0"));;

        assertTrue(thrown instanceof ResourceNotFoundException);
        assertTrue(thrown.getMessage().contains("HTTP 404 Not Found"));
    }

    @Test
    public void getQuestionnaireMatch() throws Exception {
        stubFor(get(urlPathMatching("/Questionnaire"))
                .willReturn(okJson(QUESTIONNAIRE_MATCHED_BUNDLE)
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                )
        );

        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Questionnaire questionnaire = restUtils.getQuestionnaire("canonical://uuid/Questionnaire/2761fbe9-f4d0-49c9-b96e-ae1129d1d4aa|1.0.0");

        assertEquals("questionnaire-test-system", questionnaire.getIdentifier().get(0).getSystem());
        assertEquals("questionnaire-test-value", questionnaire.getIdentifier().get(0).getValue());
    }

    @Test
    public void getQuestionnaireMalformedCannonicalNoVersion() {
        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Throwable thrown = catchThrowable(() -> restUtils.getQuestionnaire("canonical://uuid/Questionnaire/2761fbe9-f4d0-49c9-b96e-ae1129d1d4aa"));;

        assertTrue(thrown instanceof Exception);
        assertTrue(thrown.getMessage().contains("Malformed cannonical url for Questionnaire"));
    }

    @Test
    public void getQuestionnaireMalformedCannonicalNoVersion2() {
        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Throwable thrown = catchThrowable(() -> restUtils.getQuestionnaire("canonical://uuid/Questionnaire/2761fbe9-f4d0-49c9-b96e-ae1129d1d4aa|"));;

        assertTrue(thrown instanceof Exception);
        assertTrue(thrown.getMessage().contains("Malformed cannonical url for Questionnaire"));
    }

    @Test
    public void getQuestionnaireMalformedCannonicalNoVersionOrUrl() {
        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Throwable thrown = catchThrowable(() -> restUtils.getQuestionnaire("|"));;

        assertTrue(thrown instanceof Exception);
        assertTrue(thrown.getMessage().contains("Malformed cannonical url for Questionnaire"));
    }

    @Test
    public void getQuestionnaireMalformedCannonicalNoUrl() {
        FHIRRestUtils restUtils = new FHIRRestUtils(FhirContextUtil.getFhirContext(), serviceUrl, serviceUrl, serviceUrl, serviceUrl);
        Throwable thrown = catchThrowable(() -> restUtils.getQuestionnaire("|1.0.0"));;

        assertTrue(thrown instanceof Exception);
        assertTrue(thrown.getMessage().contains("Malformed cannonical url for Questionnaire"));
    }

}