package dk.s4.microservices.fhir2cda.convert;

import static dk.s4.microservices.microservicecommon.fhir.ResourceUtil.stringFromResource;


import dk.s4.hl7.cda.model.DataInputContext.PerformerType;
import dk.s4.hl7.cda.model.DataInputContext.ProvisionMethod;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.Measurement.Status;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import dk.s4.microservices.fhir2cda.service.ServiceVariables;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import java.io.IOException;
import java.util.List;
import junit.framework.TestCase;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Device.DeviceVersionComponent;
import org.hl7.fhir.r4.model.Observation;
import org.junit.ClassRule;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FHIR2CDAUtilsTest extends TestCase {
    private final static Logger logger = LoggerFactory.getLogger(FHIR2CDAUtilsTest.class);

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    public void setUp() throws Exception {
        super.setUp();

        TestUtils.readEnvironment("deploy.env", environmentVariables);
        TestUtils.readEnvironment("service.env", environmentVariables);
    }

    public void tearDown() throws Exception {
    }

    public void testBuildPersonIdentity() {
        //TODO
    }

    public void testBuildPatient() {
        //TODO
    }

    public void testBuildOrganizationIdentity() {
        //TODO
    }

    public void testBuildMeasurementNoComponents() throws IOException {
        String obsAsString = ResourceUtil.stringFromResource("Observation_weight.json");
        Observation observation = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Observation.class, obsAsString);

        FHIR2CDAUtils cdaUtils = new FHIR2CDAUtils(FhirContextUtil.getFhirContext());
        List<Measurement> measurements = cdaUtils.buildMeasurements(observation);

        assertEquals(1, measurements.size());
        Measurement measurement = measurements.get(0);

        assertEquals("82.0", measurement.getValue());
        assertEquals("188736", measurement.getCode());
        assertEquals(ServiceVariables.MDC_OID.getValue(), measurement.getCodeSystem());
        assertEquals("MDC", measurement.getCodeSystemName());
        assertEquals("kg", measurement.getUnit());
        assertEquals("MDC_MASS_BODY_ACTUAL", measurement.getDisplayName());
        assertEquals(Status.COMPLETED, measurement.getStatus());
        assertEquals(PerformerType.Citizen, measurement.getDataInputContext().getMeasurementPerformerCode());
        assertEquals(ProvisionMethod.TypedByCitizen, measurement.getDataInputContext().getMeasurementProvisionMethodCode());
    }

    public void testBuildMeasurementComponents() throws IOException {
        String obsAsString = ResourceUtil.stringFromResource("Observation_bloodpressure.json");
        Observation observation = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Observation.class, obsAsString);

        FHIR2CDAUtils cdaUtils = new FHIR2CDAUtils(FhirContextUtil.getFhirContext());
        List<Measurement> measurements = cdaUtils.buildMeasurements(observation);

        assertEquals(3, measurements.size());

        Measurement measurement = measurements.get(0);
        assertEquals("122.0", measurement.getValue());
        assertEquals("150021", measurement.getCode());
        assertEquals(ServiceVariables.MDC_OID.getValue(), measurement.getCodeSystem());
        assertEquals("MDC", measurement.getCodeSystemName());
        assertEquals("mm[Hg]", measurement.getUnit());
        assertEquals("MDC_PRESS_BLD_NONINV_SYS", measurement.getDisplayName());
        assertEquals(Status.COMPLETED, measurement.getStatus());
        assertEquals(PerformerType.Citizen, measurement.getDataInputContext().getMeasurementPerformerCode());
        assertEquals(ProvisionMethod.Electronically, measurement.getDataInputContext().getMeasurementProvisionMethodCode());

        measurement = measurements.get(1);
        assertEquals("85.0", measurement.getValue());
        assertEquals("150022", measurement.getCode());
        assertEquals(ServiceVariables.MDC_OID.getValue(), measurement.getCodeSystem());
        assertEquals("MDC", measurement.getCodeSystemName());
        assertEquals("mm[Hg]", measurement.getUnit());
        assertEquals("MDC_PRESS_BLD_NONINV_DIA", measurement.getDisplayName());
        assertEquals(Status.COMPLETED, measurement.getStatus());
        assertEquals(PerformerType.Citizen, measurement.getDataInputContext().getMeasurementPerformerCode());
        assertEquals(ProvisionMethod.Electronically, measurement.getDataInputContext().getMeasurementProvisionMethodCode());

        measurement = measurements.get(2);
        assertEquals("96.0", measurement.getValue());
        assertEquals("150023", measurement.getCode());
        assertEquals(ServiceVariables.MDC_OID.getValue(), measurement.getCodeSystem());
        assertEquals("MDC", measurement.getCodeSystemName());
        assertEquals("mm[Hg]", measurement.getUnit());
        assertEquals("MDC_PRESS_BLD_NONINV_MEAN", measurement.getDisplayName());
        assertEquals(Status.COMPLETED, measurement.getStatus());
        assertEquals(PerformerType.Citizen, measurement.getDataInputContext().getMeasurementPerformerCode());
        assertEquals(ProvisionMethod.Electronically, measurement.getDataInputContext().getMeasurementProvisionMethodCode());

    }

    public void testBuildMedicalEquipment() throws Exception {
        String deviceAsString = ResourceUtil.stringFromResource("FHIR-clean/Individual-resources/Device_WeightScale.json");
        Device device = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Device.class, deviceAsString);

        FHIR2CDAUtils cdaUtils = new FHIR2CDAUtils(FhirContextUtil.getFhirContext());
        MedicalEquipment medicalEquipment = cdaUtils.buildMedicalEquipment(device);

        logger.debug("MedicalEquipment: " + medicalEquipment.toString());

        assertEquals("528399", medicalEquipment.getMedicalDeviceCodeValue());
        assertEquals("2.16.840.1.113883.6.24", medicalEquipment.getMedicalDeviceCodeSystem());
        assertEquals("MDC_DEV_SPEC_PROFILE_SCALE", medicalEquipment.getMedicalDeviceCodeDisplayName());
        assertEquals(null, medicalEquipment.getMedComMedicalDeviceCode());
        assertEquals("A&D Medical UC-321PBT-C", medicalEquipment.getMedicalDeviceDisplayName());
        assertEquals("UC-321PBT-C", medicalEquipment.getManufacturerModelName());
        assertEquals(null, medicalEquipment.getSerialNumber());
        assertEquals("02.00, ", medicalEquipment.getSoftwareName());
    }

    public void testBuildMedicalEquipmentWithMedComCode() throws Exception {
        String deviceAsString = ResourceUtil.stringFromResource("FHIR-clean/Individual-resources/Device_WeightScale.json");
        Device device = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Device.class, deviceAsString);
        device.setManufacturer("A&D Company"); //This is the manufacturer name in the official MedCom instrument code table for this device

        FHIR2CDAUtils cdaUtils = new FHIR2CDAUtils(FhirContextUtil.getFhirContext());
        MedicalEquipment medicalEquipment = cdaUtils.buildMedicalEquipment(device);

        logger.debug("MedicalEquipment: " + medicalEquipment.toString());

        assertEquals("528399", medicalEquipment.getMedicalDeviceCodeValue());
        assertEquals("2.16.840.1.113883.6.24", medicalEquipment.getMedicalDeviceCodeSystem());
        assertEquals("MDC_DEV_SPEC_PROFILE_SCALE", medicalEquipment.getMedicalDeviceCodeDisplayName());
        assertEquals("MCI00002", medicalEquipment.getMedComMedicalDeviceCode());
        assertEquals("A&D Company UC-321PBT-C", medicalEquipment.getMedicalDeviceDisplayName());
        assertEquals("UC-321PBT-C", medicalEquipment.getManufacturerModelName());
        assertEquals("MCI00002", medicalEquipment.getSerialNumber());
        assertEquals("02.00, ", medicalEquipment.getSoftwareName());
    }

    public void testBuildAddressData() {
        //TODO
    }

    public void testBuildTelecomList() {
        //TODO
    }
}