package dk.s4.microservices.fhir2cda.convert;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import ca.uhn.fhir.context.FhirContext;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.Measurement.Type;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.microservices.fhir2cda.service.ServiceVariables;
import dk.s4.microservices.microservicecommon.TestUtils;
import dk.s4.microservices.microservicecommon.fhir.ResourceUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.hl7.fhir.r4.model.CarePlan;
import org.hl7.fhir.r4.model.ContactPoint;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointSystem;
import org.hl7.fhir.r4.model.ContactPoint.ContactPointUse;
import org.hl7.fhir.r4.model.DateTimeType;
import org.hl7.fhir.r4.model.Device;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Organization;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.Reference;
import org.hl7.fhir.r4.model.StringType;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.mockito.ArgumentMatcher;
import org.mockito.Mockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Fhir2phmrConverterTest {
    private final static Logger logger = LoggerFactory.getLogger(Fhir2phmrConverterTest.class);

    @ClassRule
    public final static EnvironmentVariables environmentVariables = new EnvironmentVariables();

    @Test
    public void testConvertIncomplete() throws Exception {
        FHIRRestUtils restUtils = mock(FHIRRestUtils.class);
        FhirContext fhirContext = FhirContextUtil.getFhirContext();
        Fhir2phmrConverter converter = new Fhir2phmrConverter(new PHMRXmlCodec(), fhirContext, restUtils, new FHIR2CDAUtils(fhirContext));
        Fhir2phmrConverter converterSpy = Mockito.spy(converter);

        PHMRDocument phmr = new PHMRDocument(new ID.IDBuilder()
                .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
                .setExtension("test-document-id")
                .setRoot(MedCom.ROOT_OID).build());
        Mockito.doReturn(phmr).when(converterSpy).buildHeader(any());
        Mockito.doNothing().when(converterSpy).buildBody(any(), any());

        String obsString = ResourceUtil.stringFromResource("Observation_weight.json");
        Throwable thrown = catchThrowable(() -> converterSpy.convert(obsString));

        assertTrue(thrown instanceof Exception);
        assertTrue(thrown.getMessage().contains("Conversion produced incomplete CDA document"));
    }

    @Test
    public void testBuildHeader() throws Exception {
        FHIRRestUtils restUtils = mock(FHIRRestUtils.class);

        Identifier ptIdentifier = new Identifier().setSystem(ServiceVariables.OFFICIAL_PATIENT_IDENTIFIER_SYSTEM.getValue()).setValue("2512489996");
        Patient patient = new Patient();
        patient.addName().addGiven("Nancy Ann Test").setFamily("Berggren");
        patient.addIdentifier(ptIdentifier);
        patient.setId("http://example.com/patient_service/1");
        when(restUtils.getPatient(any())).thenReturn(patient);

        Identifier orgIdentifier = new Identifier().setSystem(ServiceVariables.ORGANIZATION_IDENTIFIER_SYSTEM.getValue()).setValue("310541000016007");
        Organization organization = new Organization().setIdentifier(Arrays.asList(orgIdentifier));
        organization.addAddress()
                .setPostalCode("8000")
                .setCity("Aarhus")
                .setLine(Arrays.asList(new StringType("Aabogade 34")))
                .setCountry("Danmark");
        organization.addTelecom(new ContactPoint().setSystem(ContactPointSystem.PHONE).setUse(ContactPointUse.HOME).setValue("+45123456"));
        when(restUtils.getOrganization(any())).thenReturn(organization);

        CarePlan carePlan = new CarePlan().setAuthor(new Reference().setIdentifier(new Identifier()
                .setSystem("careplan-test-system")
                .setValue("careplan-test-value")).setType("CarePlan"));
        when(restUtils.getCarePlan(any())).thenReturn(carePlan);

        String obsUuid = UUID.randomUUID().toString();
        Observation observation = new Observation();
        observation.addIdentifier(new Identifier().setSystem(ServiceVariables.MEDCOM_OID.getValue()).setValue(obsUuid));
        observation.setSubject(new Reference().setIdentifier(ptIdentifier).setReference("test"));
        observation.setEffective(new DateTimeType("2015-02-07T13:28:17+02:00"));
        ArrayList<Reference> basedOnRefs = new ArrayList<Reference>();
        basedOnRefs.add(new Reference().setIdentifier(new Identifier()
                .setSystem("basedOn-test-system")
                .setValue("basedOn-test-value")).setType("CarePlan"));
        observation.setBasedOn(basedOnRefs);

        FhirContext fhirContext = FhirContextUtil.getFhirContext();
        Fhir2phmrConverter converter = new Fhir2phmrConverter(new PHMRXmlCodec(), fhirContext, restUtils, new FHIR2CDAUtils(fhirContext));
        PHMRDocument phmrDocument = (PHMRDocument) converter.buildHeader(observation);

        // PHMR to String
        PHMRXmlCodec phmrCodec = new PHMRXmlCodec();
        String phmrString = phmrCodec.encode(phmrDocument);

        logger.info("PHMR header:\n" + phmrString);

        boolean completePHMR = phmrString.contains("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>")
                && phmrString.contains("</ClinicalDocument>");
        assertTrue(completePHMR);

        assertEquals("1.2.208.184", phmrDocument.getId().getRoot());
        assertEquals(obsUuid, phmrDocument.getId().getExtension());

        assertEquals("1.2.208.176.1.2", phmrDocument.getPatient().getId().getRoot());
        assertEquals("2512489996", phmrDocument.getPatient().getId().getExtension());

        //Do not use time of Observation as document creation time
        assertNotEquals(new DateTimeType("2015-02-07T13:28:17+02:00").getValue(), phmrDocument.getEffectiveTime());

        assertEquals("310541000016007", phmrDocument.getAuthor().getId().getExtension());
        assertEquals("Aarhus", phmrDocument.getAuthor().getAddress().getCity());
        assertEquals("+45123456", phmrDocument.getAuthor().getTelecomList()[0].getValue());
        assertNull(phmrDocument.getAuthor().getPersonIdentity());

        assertEquals("328151000016009", phmrDocument.getCustodianIdentity().getId().getExtension());

        assertEquals("310541000016007", phmrDocument.getLegalAuthenticator().getId().getExtension());
        assertEquals("Christensen", phmrDocument.getLegalAuthenticator().getPersonIdentity().getFamilyName());
    }

    @Test
    public void testBuildBodyWeight() throws Exception {
        String obsString = ResourceUtil.stringFromResource("Observation_weight.json");
        Observation observation = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Observation.class, obsString);
        PHMRDocument phmrDocument = new PHMRDocument(new ID.IDBuilder()
                .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
                .setExtension(observation.getIdentifier().get(0).getValue())
                .setRoot(MedCom.ROOT_OID).build());

        FHIRRestUtils restUtils = mock(FHIRRestUtils.class);
        FhirContext fhirContext = FhirContextUtil.getFhirContext();
        Fhir2phmrConverter converter = new Fhir2phmrConverter(new PHMRXmlCodec(), fhirContext, restUtils, new FHIR2CDAUtils(fhirContext));

        String deviceAsString = ResourceUtil.stringFromResource("FHIR-clean/Individual-resources/Device_WeightScale.json");
        Device device = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Device.class, deviceAsString);
        when(restUtils.getDevice(any())).thenReturn(device);

        converter.buildBody(phmrDocument, observation);

        List<MedicalEquipment> equipmentList = phmrDocument.getMedicalEquipments();
        assertEquals(1, equipmentList.size());
        assertEquals("528399", equipmentList.get(0).getMedicalDeviceCodeValue());
        //We don't consider weight a vital sign
        assertEquals(0, phmrDocument.getVitalSigns().size());
        assertEquals(1, phmrDocument.getResults().size());
        Measurement measurement = phmrDocument.getResults().get(0);
        assertEquals("188736", measurement.getCode());
        assertEquals("2.16.840.1.113883.6.24", measurement.getCodeSystem());
        assertEquals("kg", measurement.getUnit());
        assertEquals("82.0", measurement.getValue());
        assertEquals(Type.PHYSICAL_QUANTITY, measurement.getType());
    }

    private class PhgDeviceReferenceMatcher extends ArgumentMatcher<Reference> {
        @Override
        public boolean matches(Object o) {
            if (o instanceof Reference) {
                Reference reference = (Reference)o;
                if (reference.getIdentifier().getValue().equals("C4-B3-01-D2-80-C6"))
                    return true;
            }
            return false;
        }
    }

    private class BloodPressureDeviceReferenceMatcher extends ArgumentMatcher<Reference> {
        @Override
        public boolean matches(Object o) {
            if (o instanceof Reference) {
                Reference reference = (Reference)o;
                if (reference.getIdentifier().getValue().equals("00-09-1F-FE-FF-80-1A-33"))
                    return true;
            }
            return false;
        }
    }

    @Test
    public void testBuildBodyBloodPressure() throws Exception {
        String obsString = ResourceUtil.stringFromResource("Observation_bloodpressure.json");
        Observation observation = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Observation.class, obsString);
        PHMRDocument phmrDocument = new PHMRDocument(new ID.IDBuilder()
                .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
                .setExtension(observation.getIdentifier().get(0).getValue())
                .setRoot(MedCom.ROOT_OID).build());

        FHIRRestUtils restUtils = mock(FHIRRestUtils.class);
        FhirContext fhirContext = FhirContextUtil.getFhirContext();
        Fhir2phmrConverter converter = new Fhir2phmrConverter(new PHMRXmlCodec(), fhirContext, restUtils, new FHIR2CDAUtils(fhirContext));

        String deviceAsString = ResourceUtil.stringFromResource("FHIR-clean/Individual-resources/Device_BloodPressure.json");
        Device device = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Device.class, deviceAsString);
        when(restUtils.getDevice(argThat(new BloodPressureDeviceReferenceMatcher()))).thenReturn(device);

        String phgDeviceAsString = ResourceUtil.stringFromResource("FHIR-clean/Individual-resources/Device_PHG.json");
        Device phgDevice = FhirContextUtil.getFhirContext().newJsonParser().parseResource(Device.class, phgDeviceAsString);
        when(restUtils.getDevice(argThat(new PhgDeviceReferenceMatcher()))).thenReturn(phgDevice);

        converter.buildBody(phmrDocument, observation);

        List<MedicalEquipment> equipmentList = phmrDocument.getMedicalEquipments();
        assertEquals(2, equipmentList.size());
        assertEquals("528391", equipmentList.get(0).getMedicalDeviceCodeValue());
        assertEquals("531981", equipmentList.get(1).getMedicalDeviceCodeValue());
        //Blood pressure is a vital sign
        assertEquals(3, phmrDocument.getVitalSigns().size());
        assertEquals(0, phmrDocument.getResults().size());

        Measurement measurement = phmrDocument.getVitalSigns().get(0);
        assertEquals("150021", measurement.getCode());
        measurement = phmrDocument.getVitalSigns().get(1);
        assertEquals("150022", measurement.getCode());
        measurement = phmrDocument.getVitalSigns().get(2);
        assertEquals("150023", measurement.getCode());
    }

    @BeforeClass
    public static void beforeClass() throws IOException {
        TestUtils.readEnvironment("service.env", environmentVariables);
        TestUtils.readEnvironment("deploy.env", environmentVariables);
    }
}