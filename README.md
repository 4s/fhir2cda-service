# FHIR resource To CDA document Service:

| Summary                          |                                                                                                                     |
|----------------------------------|---------------------------------------------------------------------------------------------------------------------|
| Service name (computer friendly) | fhir2cda-service                                                                                                        |
| Lastest version                  | 2.5.0                                                                                                              |
| Maturity level                   | Concept [ ] Prototype [ ] Demonstration [x] Product [ ]                                                                                                            |
| Used externally                  | No                                                                   |
| Database                         | No                                                                                                                  |
| RESTful dependencies             | outcome-service, patientcare-service and organizational-service   |
| Messaging dependencies           | Kafka                                                                                                               |
| Other dependencies               | User context service (user context info from session ID)                                                            |
| License                          | Apache 2.0                                                                                                          |

The purpose of this service is to convert HL7 FHIR resources HL7 CDA documents, specifically it converts 
FHIR Observation and QuestionniareResponse resources into HL7 PHMR and QRD documents. 
The service is designed for use in a microservice based system as the one described at https://bitbucket.org/4s/gmk-gravide-med-komplikationer.

The fhir2cda-service subscribes to Kafka messages containing FHIR Observation and QuestionnaireResponse resources, converts these 
resources to HL7 PHMR and QRD format and writes messages containing the converted data back to Kafka.

The Fhir2cda-service defaults to subscribing on messages from Kafka on the topic pattern 
```
DataCreated_FHIR_Observation(_.*)*|DataUpdated_FHIR_Observation(_.*)*|DataCreated_FHIR_Questionnaire|DataUpdated_FHIR_Questionnaire
```  

The service writes the converted data on Kafka on a topic named "DataConverted_CDA_\<CDA document type\>[_\<measurement code\>"], 
where \<CDA document type\> is either "PHMR" or "QRD". The \<measurement code\> part is only used with PHMR.

See also _Scoping and constaints_ below. 

## Further documentation

Design and external API documentation: See https://bitbucket.org/4s/fhir2cda-service/wiki/Home.

Javadoc can be generated using the command:
```
javadoc -sourcepath src/main/java -subpackages dk.s4.microservices.fhir2cda -d <destination-folder>
```

## Prerequisites
You need to have the following software installed:

  * Java 1.8.0+
  * Maven 3.1.1+
  * Docker 1.13+
  
The project depends on git submodules (ms-scripts). To initialize these you need to execute:

```bash
git submodule update --init
git submodule foreach git pull origin master
```
  
In the service.env file you need to set ```KAFKA_BOOTSTRAP_SERVER``` to point to a running Kafka broker. By default it is set
to point to ```kafka:9092``` which is the address Kafka will run at if you run it locally via this project: http://4s-online.dk/wiki/doku.php?id=4smicroservices:kafka.
  
## Building and running
The simplest way to build the service is to use the `buildall.sh` script:

`/scripts/buildall.sh`

This will build the service as a WAR file and add this to a Jetty based docker image.

To run the service:

`docker-compose up`

## Environment variables

The files `service.env` and `deploy.env` define all environment variables used by this microservice. `deploy.env` defines
service URLs and log levels that depend on your particular deployment.

TODO: Table documenting individual environment variables.
 
## Logging with fluentd
If you want use the fluentd logging driver (if you for instance have setup fluentd, elasticsearch and Kibana) 
instead of the standard json-file one start the service like this:

```bash
docker-compose -f docker-compose.yml -f docker-compose.fluentd-logging.yml up
```

##Scoping and constraints

###Header
__Generic__

_setId and versionNumber:_

We do not put setId and versionNumber in header. Optional SetId could identify this document but remain constant
through updates. When the document would be updated, the associated versionNumber would be incremented instead.
However, this service does not have state and has no way of knowing if a previous version of the PHMR document 
already exists / has been generated.

__QRD__
  - We only support Patient type authors (not Practitioner or RelatedPerson)
  - We only support Patient as data enterer (not Practitioner or RelatedPerson)

###Body
__Generic__
  - We assume that FHIR resource references are logical (i.e. af reference that contains a FHIR Identifier and a type)
  - References between PHMR and QRD have not been implemented (DK profiling is ongoing)

__PHMR__
  - We require MDC codes on measurements (PHMR requires either MDC or LOINC)
  - CTGs are not supported
  - Organizations must have a DK SOR code
  - Weight is placed in results section, not in vital signs
  - We only support FHIR observations with ValueQuantity or with components with ValueQuantity. Units must be specified in UCUM.
  - We only support FHIR observations with effectiveDateTime (not with any of the other effect[x] types). This is a restriction in the data model of the CDA builder.

_Blood pressure measurements:_
 
In FHIR this is an observation with 3 components (systolic, diastolic and mean). In a PHMR this ought to result in an entry with three components.
However this is not supported by the data model used in the CDA builder and this is also not how it's modelled in the official MedCom examples:
http://svn.medcom.dk/svn/releases/Standarder/HL7/PHMR/Eksempler/Ex2-Typing_error.xml. Here each component of the measurement has it's own entry.
The latter is also the way this service generates PHMR.

_CONF-PHMR-DK-34:_
CONF-PHMR-DK-34 mandates a translation from MDC code to MedCom instrument code. However, this is not legal. The original code in MDC describes a
generic device type, while the translated MedCom value describes a particular model. A translation is NOT allowed higher granularity than the original
concept. A general solution to this would be to instead move the MedCom instrument code elsewhere - e.g. to the participantRole/id.
In this service we have chosen to give the MedCom instrument code if it can be looked up 
(in https://svn.medcom.dk/svn/releases/Standarder/POCT-Hjemmemonitorering/MedCom%20Instrument%20Codes.xlsx). If the MedCom instrument code can not be 
looked up from the device details we insert the MDC code (with no translation).  

__QRD__

  - Multiple choice questions are not supported
  - FHIR Quantity probably ought to be translated to value xsi:type="PQ" (PQ=Physical Quantity), but according to the QRD profile
    only INT, REAL or TS are allowed as xsi:type on numeric observations. Furthermore, no "physical quantity" response exists in the profile. 

_Id and code of responses:_

For every question / response the QRD profile assumes there will be a coding, so that you can state a particular code for each particular question.
Within the QRD this is reflected in the "id" and "code" parts of the "observation".

However, for the FHIR QuestionnaireResponse resources, that this service receives and translates, the source of questions is the FHIR Questionnaire pointed to
by the cannonical reference of the QuestionnaireResponse resource. And each question within the FHIR Questionnaire has a linkId. So, in our translation
each of the responses in the QRD will use externalObservation to link to the linkId of the FHIR Questionnaire, that can be retrieved from a FHIR server.

The deployment of the service needs to setup environment variables that point to assigning authority, root oid, code system and code system name that 
reflect a mapping system like the one described above. This is done using the following environment variables (with example values):

```
QRD_ID_ASSIGNING_AUTHORITY_NAME=Region Midtjylland
QRD_ID_ROOT=1.2.208.180.x.x
QRD_CODE_CODE_SYSTEM=1.2.208.180.x.x
QRD_CODE_SYSTEM_NAME=RM FHIR Questionnaire mapping 
```

Below a full example of numeric response with link to (linkId of) question in FHIR Questionnaire. Linking is based on 
https://svn.medcom.dk/svn/releases/Standarder/HL7/CDA-XREF/Notat%20-%20Krydsreferencer%20mellem%20CDA%20dokumenter-0.9.3.pdf.

```
	<component typeCode="COMP" contextConductionInd="true">
		<sequenceNumber value="2"/>
		<observation classCode="OBS" moodCode="EVN">
			<!--templateID for the Numeric Response Pattern-->
			<templateId root="2.16.840.1.113883.10.20.33.4.4"/>
			<id assigningAuthorityName="Region Midtjylland" extension="linkId-1" root="1.2.208.180.x.x"/>
			<code code="linkId-1" codeSystem="1.2.208.180.x.x" codeSystemName="Region Midtjylland FHIR Questionnaire mapping">
				<originalText>Skriv måned - skriv nul, hvis du ikke husker dette</originalText>
			</code>
			<value xsi:type="INT" value="12"/>
			<reference typeCode="REFR">
				<templateId root="1.2.208.184.6.1"/>
				<externalObservation classCode="DOC">
					<id root="1.2.208.184.5.3"
						extension="http://telemed.rm.dk/gmk-employee-bff/baseR4/Questionnaire?version=1.0.0&amp;url=canonical://uuid/Questionnaire/2761fbe9-f4d0-49c9-b96e-ae1129d1d4aa"/>
					<id root="1.2.208.184.100.2" extension="linkId-1" />
					<id root="1.2.208.184.5" extension="3" />
					<code code="Questionnaire" codeSystem="2.16.840.1.113883.6.306" displayName="fhir-resource-types"/>
				</externalObservation>
			</reference>
		</observation>
	</component>
```
##TODO

__CONF-PHMR-DK-36__
Can we retrieve therapeutic reference ranges from GMKs FHIR ThresholdSet?  

CONF-PHMR-DK-36 (http://svn.medcom.dk/svn/releases/Standarder/HL7/PHMR/Dokumentation/PHMR-DK-Profile-v1.3.pdf):
"Observations MAY specify therapeutic reference ranges know in DK as
Red and Yellow reference ranges. If specified a templateId shall be
present where @root is 1.2.208.184.11.1.2, and a code element 
SHALL be present where @codeSystem is 1.2.208.184.100.1 MedCom
Message Codes.
The reference ranges are specified as interval values where one and only
one of the two attributes @low=low limit, @high=high limit MAY be left
out."

   
Issues:
======

  - DataInputContext does not map well to FHIR observation.performer and observation.extension.dataEntryMethod
  - What to do about human readable representations? We don't have them in the FHIR resources