FROM openjdk:11

LABEL Description="Dockerfile for fhir2cda service" Vendor="Alexandra Institute A/S" Version="1.0"

WORKDIR /opt/s4/service

ADD scripts/health.sh .
ADD target/service-jar-with-dependencies.jar .

ENV JAVA_OPTIONS ""
ENTRYPOINT ["/bin/bash", "-c", "java ${JAVA_OPTIONS} -XX:+PrintFlagsFinal -XX:+UseContainerSupport -jar service-jar-with-dependencies.jar"]

